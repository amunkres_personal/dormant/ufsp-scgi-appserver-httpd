#include <string>
#include <limits>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stdint.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "common.h"

using namespace std;

static sockaddr_un sun_sizecheck;

int main(int argc, char **argv)
{
  if (2 > argc)
  {
    fprintf(stderr, "Usage: deleteuser USERNAME\n");
    exit(42);
  }
  
  // Apparently, we need to fully become root in order to use userdel non-interactively.
  passwd *super_pw_entry(getpwuid(0));
  if (NULL == super_pw_entry)
  {
    fprintf(stderr, "Error: the superuser does not exist in system password database!?\n");
    exit(42);
  }
  if (-1 == initgroups(super_pw_entry->pw_name, super_pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: while becoming superuser, initgroups(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setgid(super_pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: while becoming superuser, setgid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setuid(super_pw_entry->pw_uid))
  {
    fprintf(stderr, "Error: while becoming superuser, setuid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  
  // Check that we won't exceed the maximum message size before we do anything else.
  size_t username_len(strlen(argv[1]));
  const size_t msg_len((DATA_BEGIN_OFFSET * sizeof(u_int32_t)) + username_len);
  if (size_t(BUF_SIZE) < msg_len)
  {
    fprintf(stderr, "Error: username is too long\n");
    exit(42);
  }
  
  // Create socket for sending user creation message to server
  int um_fd(socket(AF_LOCAL, SOCK_DGRAM, 0));
  int flags(fcntl(um_fd, F_GETFD));
  flags |= FD_CLOEXEC;
  fcntl(um_fd, F_SETFD, flags);
  sockaddr_un um_addr;
  um_addr.sun_family = AF_UNIX;
  string um_path(WEB_ROOT_DIR UM_COMM_DIR_SUFFIX "/um_socket");
  if (sizeof(sun_sizecheck.sun_path) < um_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", um_path.c_str(), sizeof(sun_sizecheck.sun_path));
    exit(42);
  }
  strncpy(um_addr.sun_path, um_path.c_str(), sizeof(sun_sizecheck.sun_path));
  int retval(connect(um_fd, static_cast<sockaddr *>(static_cast<void *>(&um_addr)), sizeof(um_addr)));
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not connect to pathname \"%s\" (%s)\n", um_path.c_str(), strerror(errno));
    exit(42);
  }
  
  // WARNING! We must check that the user's login group is "projusers" before deleting!
  group *gr_entry(getgrnam("projusers"));
  if (NULL == gr_entry)
  {
    fprintf(stderr, "Error: could not find group \"projusers\" (%s)\n", strerror(errno));
    exit(42);
  }
  passwd *pw_entry(getpwnam(argv[1]));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: user \"%s\" does not exist in system password database\n", argv[1]);
    exit(42);
  }
  else if (gr_entry->gr_gid != pw_entry->pw_gid)
  {
    fprintf(stderr, "Error: user \"%s\" has primary GID %d instead of %d, so we're not allowed to delete it\n", argv[1], pw_entry->pw_gid, gr_entry->gr_gid);
    exit(42);
  }
  
  // Use "userdel" to delete a user.
  pid_t pid(fork());
  if (0 == pid)
  {
    static char *const envp[] = {static_cast<char *>(NULL)};
    execle("/usr/sbin/userdel", "/usr/sbin/userdel", "--remove", argv[1], static_cast<char *>(NULL), envp);
    fprintf(stderr, "Error: failed to exec /usr/sbin/userdel (%s)\n", strerror(errno));
    exit(42);
  }
  int status;
  retval = wait(&status);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: wait(2) for userdel process failed (%s)\n", strerror(errno));
  }
  if (WIFEXITED(status))
  {
    retval = WEXITSTATUS(status);
    switch (retval)
    {
      case 42:
        fprintf(stderr, "Error: subprocess failed to exec /usr/sbin/userdel\n");
        exit(42);
        break;
        
      case 0:
        // It succeeded. Only in this case do we continue.
        break;
        
      default:
        fprintf(stderr, "Error: /usr/sbin/userdel subprocess error-exited with status %d\n", retval);
        exit(42);
        break;
    }
  }
  
  // Try to delete the user's socket and socket dir.
  string scgi_dir(WEB_ROOT_DIR SCGI_COMM_DIR_SUFFIX "/");
  scgi_dir.append(argv[1]);
  scgi_dir.append(SCGI_USER_DIR_SUFFIX);
  string scgi_socket_path(scgi_dir);
  scgi_socket_path.append(SCGI_SOCKET_SUFFIX);
  retval = unlink(scgi_socket_path.c_str());
  if (-1 == retval)
  {
    fprintf(stderr, "Warning: failed to delete socket \"%s\" (%s)\n", scgi_socket_path.c_str(), strerror(errno));
  }
  retval = rmdir(scgi_dir.c_str());
  if (-1 == retval)
  {
    fprintf(stderr, "Warning: failed to delete directory \"%s\" (%s)\n", scgi_dir.c_str(), strerror(errno));
  }
  // NOTE: For now, we won't require that these deletes succeed. (WARNING: This could potentially lead to a DoS attack where a user prevents their name from being reused by creating other files in their socket directory. Maybe we should exec "rm -r" instead?)
  
  // Prepare the message to send to the server.
  char temp_buf[msg_len];
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + ACTION_CODE_OFFSET) = u_int32_t(DELETE_USER);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET) = u_int32_t(username_len);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + PASSWORD_LEN_OFFSET) = u_int32_t(0);
  memcpy(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET), argv[1], username_len);
  
  // Send it.
  retval = send(um_fd, static_cast<void *>(temp_buf), msg_len, 0);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: send(2) to send message to server failed (%s)\n", strerror(errno));
    exit(42);
  }
  
  // We're done.
  exit(0);
}
