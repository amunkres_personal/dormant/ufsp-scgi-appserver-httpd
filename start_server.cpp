#include <string>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

using namespace std;

static sockaddr_un sun_sizecheck;

int main(int argc, char **argv)
{
  if (2 > argc)
  {
    fprintf(stderr, "Usage: start_server UID\n");
    exit(42);
  }
  
  istringstream uid_conv(argv[1]);
  uid_t uid;
  uid_conv >> uid;
  
  passwd *pw_entry(getpwuid(uid));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: UID %d does not exist in system password database\n", uid);
    exit(42);
  }
  
  // WARNING! Although the HTTP server is supposed to have already checked that the user's login group is "projusers", we still should check for that here. This is because, in the case that the HTTP server has been compromised, performing the check here limits the attacker to only being able to become any of the web-users (users in group "projusers"), whereas not performing the check here allows the attacker to become *any user at all*!
  group *gr_entry(getgrnam("projusers"));
  if (NULL == gr_entry)
  {
    fprintf(stderr, "Error: could not find group \"projusers\" (%s)\n", strerror(errno));
    exit(42);
  }
  if (gr_entry->gr_gid != pw_entry->pw_gid)
  {
    fprintf(stderr, "Error: uid %d has primary GID %d instead of %d, so we're not allowed to use its credentials\n", uid, pw_entry->pw_gid, gr_entry->gr_gid);
    exit(42);
  }
  
  // Set all of the user and group credentials to those for the authenticated user.
  if (-1 == initgroups(pw_entry->pw_name, pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: initgroups(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setgid(pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: setgid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setuid(pw_entry->pw_uid))
  {
    fprintf(stderr, "Error: setuid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  
  // Execute the SCGI server.
  string socket_path(WEB_ROOT_DIR SCGI_COMM_DIR_SUFFIX "/");
  socket_path.append(pw_entry->pw_name);
  socket_path.append(SCGI_USER_DIR_SUFFIX SCGI_SOCKET_SUFFIX);
  if (sizeof(sun_sizecheck.sun_path) < socket_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", socket_path.c_str(), sizeof(sun_sizecheck.sun_path));
    exit(42);
  }
  static char *const envp[] = {static_cast<char *>(NULL)};
  execle(WEB_ROOT_DIR "/bin/scgi_gateway.py", WEB_ROOT_DIR "/bin/scgi_gateway.py", socket_path.c_str(), static_cast<char *>(NULL), envp);
  fprintf(stderr, "Error: failed to exec the SCGI server (%s)\n", strerror(errno));
  exit(42);
}
