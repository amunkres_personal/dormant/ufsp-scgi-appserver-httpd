#include <string>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stdint.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <errno.h>

#include "common.h"

using namespace std;

static sockaddr_un sun_sizecheck;

int main(int argc, char **argv)
{
  if (1 > argc)
  {
    fprintf(stderr, "Usage: changeownpassword NEWPASSWORD\n");
    exit(42);
  }
  
  // Create socket for sending user creation message to server
  int um_fd(socket(AF_LOCAL, SOCK_DGRAM, 0));
  // NOTE: We don't need to set FD_CLOEXEC here, because we don't exec.
  sockaddr_un um_addr;
  um_addr.sun_family = AF_UNIX;
  string um_path(WEB_ROOT_DIR UM_COMM_DIR_SUFFIX "/um_socket");
  if (sizeof(sun_sizecheck.sun_path) < um_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", um_path.c_str(), sizeof(sun_sizecheck.sun_path));
    exit(42);
  }
  strncpy(um_addr.sun_path, um_path.c_str(), sizeof(sun_sizecheck.sun_path));
  int retval(connect(um_fd, static_cast<sockaddr *>(static_cast<void *>(&um_addr)), sizeof(um_addr)));
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not connect to pathname \"%s\" (%s)\n", um_path.c_str(), strerror(errno));
    exit(42);
  }
  
  // Determine which user we're supposed to change the password for from our real UID.
  // NOTE: The real UID will be that of authenticated user, and the effective UID will be the user account under which the HTTP server process runs. (See the "printid.c" program from Wikipedia for a demonstration of how the setuid bit affects real and effective UIDs. In short, the effective UID is the one that is set, and the real UID is unchanged.)
  uid_t my_realuid(getuid());
  passwd *pw_entry(getpwuid(my_realuid));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: UID %d does not exist in system password database\n", my_realuid);
    exit(42);
  }
  
  // WARNING! We must check that the user's login group is "projusers" before deleting!
  group *gr_entry(getgrnam("projusers"));
  if (NULL == gr_entry)
  {
    fprintf(stderr, "Error: could not find group \"projusers\" (%s)\n", strerror(errno));
    exit(42);
  }
  gid_t our_gid(gr_entry->gr_gid);
  if (our_gid != pw_entry->pw_gid)
  {
    fprintf(stderr, "Error: password for user \"%s\" has primary GID %d, but needs to have %d instead\n", pw_entry->pw_name, pw_entry->pw_gid, our_gid);
    exit(42);
  }
  
  // Prepare the message to send to the server.
  size_t username_len(strlen(pw_entry->pw_name)), password_len(strlen(argv[1]));
  if (size_t(BUF_SIZE) < (username_len + (DATA_BEGIN_OFFSET * sizeof(u_int32_t))))
  {
    fprintf(stderr, "Error: username is too long\n");
    exit(42);
  }
  if (size_t(BUF_SIZE) < (password_len + (DATA_BEGIN_OFFSET * sizeof(u_int32_t))))
  {
    fprintf(stderr, "Error: password is too long\n");
    exit(42);
  }
  const size_t msg_len((DATA_BEGIN_OFFSET * sizeof(u_int32_t)) + username_len + password_len);
  if (size_t(BUF_SIZE) < msg_len)
  {
    fprintf(stderr, "Error: username and password together are too long\n");
    exit(42);
  }
  char temp_buf[msg_len];
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + ACTION_CODE_OFFSET) = u_int32_t(CHANGE_PASSWORD);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET) = u_int32_t(username_len);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + PASSWORD_LEN_OFFSET) = u_int32_t(password_len);
  memcpy(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET), pw_entry->pw_name, username_len);
  memcpy(static_cast<void *>(static_cast<char *>(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET)) + username_len), argv[1], password_len);
  
  // Send it.
  retval = send(um_fd, static_cast<void *>(temp_buf), msg_len, 0);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: send(2) to send message to server failed (%s)\n", strerror(errno));
    exit(42);
  }
  
  // We're done.
  exit(0);
}
