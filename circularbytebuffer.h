#ifndef _CIRCULARBYTEBUFFER_H
#define _CIRCULARBYTEBUFFER_H

#include <sys/types.h>

class CircularByteBuffer
{
  public:
    CircularByteBuffer(size_t bufsize);
    CircularByteBuffer();
    CircularByteBuffer(const CircularByteBuffer &orig);
    ~CircularByteBuffer();
    size_t amount() const;
    size_t write(const char *in, size_t max);
    size_t read(char *out, size_t max, unsigned int flags = 0);
    void clear();
    size_t maxAmount() const;
    void setTrace(bool trace);
  private:
    void delayedInit();
    size_t m_bufsize, readpoint, writepoint;
    char *buffer;
    bool m_trace;
};

const unsigned int NO_COPY = 0x1;   // Do not actually copy the characters into "out" buffer.
const unsigned int NO_MODIFY = 0x2; // Do not actually remove the characters from the CircularByteBuffer.

#endif
