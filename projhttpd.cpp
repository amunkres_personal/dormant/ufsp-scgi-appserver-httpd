#include <string>
#include <map>
#include <set>
#include <sstream>
#include <algorithm>
#include <locale>
#include <queue>
#include <list>
#include <limits>

#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <regex.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include <db_cxx.h>
#include <b64/decode.h>

#include "circularbytebuffer.h"
#include "common.h"

template <class charT>
charT headerToEnvVar(charT c)
{
  return (('-' == c) ? '_' : ::toupper(c));
}
/*
char headerToEnvVar(char c)
{
  return (('-' == c) ? '_' : toupper<char>(c));
}
*/

using namespace std;

class FdState;
class ProcessManager;

void createSCGIConnState(int scgi_fd, int client_fd, map<int, FdState> &fd_map, int epollfd);

int attemptConnect(int sockfd, const sockaddr *addr, socklen_t addrlen);
int attemptClose(int fd);
int attemptAccept(int sockfd, sockaddr *addr, socklen_t *addrlen);
ssize_t attemptSend(int sockfd, const void *buf, size_t len, int flags);
ssize_t attemptRecv(int sockfd, void *buf, size_t len, int flags);

void handleReceivedHTTPRequestFragment(int epollfd, map<int, FdState> &fd_map, ProcessManager &proc_mgr, FdState *state, int found_fd, const gid_t projusers_gid, const regex_t &req_line_pat, const regex_t &hdr_line_pat, const regex_t &auth_val_pat, const regex_t &basic_val_pat, regmatch_t *matches, DbEnv &auth_db_env, Db *auth_db, Dbt &key, Dbt &value, char *temp_buf, char *temp_buf_2, char *listen_host, char *listen_port, const char *https_indicator);

void respond401AuthReq(int epollfd, FdState *state);
void respond400BadReq(int epollfd, FdState *state);
void respond501MethodNotImpl(int epollfd, FdState *state);
void respond501AuthNotImpl(int epollfd, FdState *state);

static sockaddr_un sun_sizecheck;

enum conn_phase {PARSE_REQUEST, WAIT_FOR_SCGI, FWD_REQUEST, FWD_RESPONSE_HDR, FWD_RESPONSE_BODY, ERROR_CLOSE, ERROR_NOCLOSE, SCGI_REQUEST_VARS, SCGI_REQUEST_BODY, SCGI_RESPONSE_HDR, SCGI_RESPONSE_BODY, SCGI_ZOMBIE, LISTEN, USER_MANAGER};

/*
 * This is what the "phases" listed above mean:
 *
 * Phases for connection to HTTP client:
 * PARSE_REQUEST: We are reading in the HTTP header lines and parsing them. (We read them into a "linear queue".) We pick next phase based on header contents (the same rules for choosing next phase are used here as are used in WAIT_FOR_SCGI phase).
 * WAIT_FOR_SCGI: We are waiting for the "alarm handler" to establish a connection for us to the appropriate SCGI server. Once it does, it will transition this connection into FWD_REQUEST phase (if doing POST and we haven't alredy read all of the request body from the client conn fd) or FWD_RESPONSE_HDR phase (if doing GET, or if doing POST and the entirety of the request body has already been read in from the client conn fd).
 * FWD_REQUEST: We are reading POST data from the client as it becomes available and putting it into a CircBuf. If the CircBuf becomes full, stop epoll from listening for events on our fd (until the CircBuf is not full). We have an associated fd, which is a conn to SCGI server that is in SCGI_REQUEST_VARS phase or SCGI_REQUEST_BODY phase. (When entering FWD_REQUEST phase, the associated fd conn will be opened.) If the CircBuf is empty when we put data into it, we must make the epoll resume listening for events on the associated fd. We go to FWD_RESPONSE phase next.
 * FWD_RESPONSE_HDR: We are taking a (pre-assembled) set of HTTP response from the "linear queue" response_hdr_to_send and sending it to the client. We have an associated fd, which is a conn to SCGI server that is in one of these phases: SCGI_REQUEST_VARS, SCGI_REQUEST_BODY, SCGI_RESPONSE_HDR, SCGI_RESPONSE_BODY, or SCGI_ZOMBIE. Initially (upon entering this phase) we stop epoll from listening for events on our fd; our associated conn will make epoll reume watching our fd once it has populated response_hdr_to_send. When finished we transition either to FWD_RESPONSE_BODY phase (in the case that the response body size is non-zero) or PARSE_REQUEST phase (in the case that the response body size is zero; we must also deallocate what remains of our associated SCGI conn in this case).
 * FWD_RESPONSE_BODY: We are taking an HTTP response body from the CircBuf response_queue (removing from the CircBuf only as much as was sent each time) and sending it to the client. Our associated conn at this point is in SCGI_RESPONSE_BODY phase or SCGI_ZOMBIE phase. If the CircBuf becomes empty, stop epoll from listening for events on our fd (until the CircBuf is not empty). When finished, we close the associated fd and delete its state, then go to PARSE_REQUEST phase.
 * ERROR_CLOSE: We are taking a (pre-assembled) error response string from a "linear queue" and sending it to the client (advancing the queue only as must as was sent each time). When finished, we close our own fd and delete our state.
 * ERROR_NOCLOSE: The only difference from ERROR_CLOSE is that, when finished, we go to PARSE_REQUEST phase.
 *
 * Phases for connection to SCGI server:
 * SCGI_REQUEST_VARS: We are taking a (pre-assembled) "variables" (or "header") portion of an SCGI request from a "linear queue" and sending it to the server (removing from the "linear queue" only as much as was sent each time). Our associated fd is a conn to HTTP client which may be in either FWD_REQUEST phase (in the case of a POST request) or FWD_RESPONSE phase (in the case of a GET request or a POST request which has finished reading the POST data). When the "linear queue" becomes empty, we proceed to the next phase. In the case that our associated client conn is for a "POST" request, the next phase will be SCGI_REQUEST_BODY; otherwise, it will be SCGI_RESPONSE_HDR.
 * SCGI_REQUEST_BODY: We are taking the "body" portion of an SCGI request from a CircBuf and sending it to the server (removing from the CircBuf only as much as was sent each time). If the CircBuf becomes empty, stop epoll from listening for events on our fd (until the CircBuf is not empty). Our associated fd is a conn to HTTP client which is in FWD_REQUEST phase. If the CircBuf is full when we take data from it, we must make the epoll resume listening for events on the associated fd. Once we have written "CONTENT_LENGTH" amount of bytes to our fd, we then proceed to SCGI_RESPONSE phase.
 * SCGI_RESPONSE_HDR: We are reading the SCGI response header lines into the "linear queue" incoming_data. Each time, we scan the newly-arrived data (and the three bytes before it) to see if we've yet encountered the end-of-headers byte sequence \r\n\r\n. Once we reach the end of the headers, we parse them and transform them into a set of HTTP response headers, which we place into the "linear queue" response_hdr_to_send that belongs to our associated client conn. Any data that's past the \r\n separator we put into the CircBuf response_queue that belongs to our associated conn. Finally, we transition into SCGI_RESPONSE_BODY phase.
 * SCGI_RESPONSE_BODY: We are reading the "body" of the SCGI response (which, if I understand correctly, corresponds *exactly* to the body of the HTTP response we want to send) and putting it into the CircBuf response_queue that belongs to our associated client conn. If the CircBuf becomes full, stop epoll from listening for events on our fd (until the CircBuf is not full). Our associated fd is a conn to HTTP client which is in FWD_RESPONSE state. If the CircBuf is empty when we put data into it, we must make the epoll resume listening for events on the associated fd. When finished, we transition to SCGI_ZOMBIE phase, and we also make our associated client conn "wake up" by making epoll resume listening for events on its fd (that is, if it's not already listening). (The SCGI server will close our conn when it's done sending; that's how we know when we're done.)
 * SCGI_ZOMBIE: We don't do anything ourself, but rather we wait for our associated fd to delete our state.
 *
 * Other phases:
 * LISTEN: The TCP listener socket's fd remains in this phase always.
 */

static bool alarm_happened, alarm_running, interrupt_happened, terminate_happened, sigchld_happened;

void setAlarmHappenedFlag(int signum)
{
  //static const char alarm_msg[] = "DEBUG: SIGALRM received\n";
  //write(2, alarm_msg, sizeof(alarm_msg));
  alarm_happened = true;
}

void setInterruptHappenedFlag(int signum)
{
  interrupt_happened = true;
}

void setTerminateHappenedFlag(int signum)
{
  terminate_happened = true;
}

/*
void reapSubprocess(int signum)
{
  int status;
  waitpid(-1, &status, WNOHANG);
}
*/

void setSubprocessExitedFlag(int signum)
{
  sigchld_happened = true;
}

class FdState
{
  public:
    FdState(__uint32_t events, int fd, conn_phase initial_phase);
    
    epoll_event event;
    conn_phase phase;
    string incoming_data, current_request_text, response_text;
    size_t next_chr_to_read, separator_pos, initial_amount;
    bool need_to_recv, doing_post, found_status;
    string scgi_incoming_data;
    size_t scgi_next_chr_to_read;
    bool scgi_need_to_recv;
    int associated_fd; // NOTE: for conn to client, this is SCGI server fd; for conn to SCGI server, this is client fd
    string scgi_vars_to_send;
    size_t scgi_vars_bytes_written;
    CircularByteBuffer post_queue;
    size_t post_content_length, post_bytes_read, post_bytes_written;
    string response_hdr_to_send;
    size_t response_hdr_bytes_written;
    CircularByteBuffer response_queue;
    size_t error_response_bytes_written;
    string username;
    // bool scgi_response_finished; // use SCGI_ZOMBIE instead
    sockaddr_in client_addr;
    bool close_on_finish;
};

FdState::FdState(__uint32_t events, int fd, conn_phase initial_phase):
  incoming_data(),
  next_chr_to_read(0),
  associated_fd(-1),
  post_queue(BUF_SIZE),
  response_queue(BUF_SIZE),
  need_to_recv(true),
  scgi_incoming_data(),
  scgi_next_chr_to_read(0),
  scgi_need_to_recv(true),
  found_status(false),
  close_on_finish(false)
{
  event.events = events;
  event.data.fd = fd;
  phase = initial_phase;
}

class ProcessManager
{
  public:
    int getFdForUser(int connection_fd, uid_t user, const string &username/*, map<int, FdState> &fd_map, int epollfd*/);
    void handleAlarm(map<int, FdState> &fd_map, int epollfd);
  private:
    static void spawnProcess(uid_t user);
    static void populateAddr(sockaddr_un &addr, const string &username);
    
    map<uid_t, set<int> > waiting_conns;
};

void ProcessManager::populateAddr(sockaddr_un &addr, const string &username)
{
  addr.sun_family = AF_UNIX;
  passwd *pw_entry(getpwnam(username.c_str()));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: user \"%s\" does not exist in system password database\n", username.c_str());
    abort();
  }
  string socket_path(WEB_ROOT_DIR SCGI_COMM_DIR_SUFFIX "/");
  socket_path.append(pw_entry->pw_name);
  socket_path.append(SCGI_USER_DIR_SUFFIX SCGI_SOCKET_SUFFIX);
  if (sizeof(sun_sizecheck.sun_path) < socket_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", socket_path.c_str(), sizeof(sun_sizecheck.sun_path));
    abort();
  }
  strncpy(addr.sun_path, socket_path.c_str(), sizeof(sun_sizecheck.sun_path));
}

int ProcessManager::getFdForUser(int connection_fd, uid_t user, const string &username)
{
  // Try to open a connection to the server for this user.
  int scgi_fd(socket(AF_LOCAL, SOCK_STREAM, 0));
  int flags(fcntl(scgi_fd, F_GETFD));
  flags |= FD_CLOEXEC;
  fcntl(scgi_fd, F_SETFD, flags);
  // NOTE: We don't set O_NONBLOCK on it until it's connected. (This is because connect(2) should be fast on an AF_LOCAL socket, and it's easier to not have to be prepared to handle EINPROGRESS error status.)
  
  sockaddr_un addr;
  populateAddr(addr, username);
  
  // NOTE: Currently, we assume that the server process is not running iff (we failed to connect to it AND no "waiting list" for it exists). We don't keep track of the PIDs of the server processes, nor do we do any kind of wait() to catch crashed/exited server processes.
  // TODO: There should at least be some kind of "reaper" to get rid of zombie processes.
  
  if (-1 == attemptConnect(scgi_fd, static_cast<sockaddr *>(static_cast<void *>(&addr)), sizeof(addr)))
  {
    int saved_errno(errno);
    fprintf(stderr, "Warning: failed to connect to SCGI server at socket \"%s\" (%s); will try to start a server instance\n", string(addr.sun_path, sizeof(addr.sun_path)).c_str(), strerror(saved_errno));
    attemptClose(scgi_fd);
    // Connection attempt failed. Put this client connection on the "waiting list" for its UID.
    map<uid_t, set<int> >::iterator iter(waiting_conns.find(user));
    if (iter == waiting_conns.end())
    {
      // There currently does not exist a waiting list for this UID, but (as we already know) the connection attempt failed. That probably means that there is no server running for this user. So, we must start one.
      spawnProcess(user);
      
      // We must create a waiting list for this UID.
      iter = (waiting_conns.insert(pair<uid_t, set<int> >(user, set<int>()))).first;
      
      // We must start the alarm running if it isn't already.
      //fprintf(stderr, "DEBUG: alarm_running flag is currently set to %d\n", alarm_running);
      if (!alarm_running)
      {
        //fprintf(stderr, "DEBUG: starting alarm %d\n", alarm_running);
        alarm_running = true;
        alarm(1);
      }
    }
    else
    {
      // A waiting list for this UID exists. That means that an attempt to start the server has already been made. In this case, we don't need to do anything except for adding the client connection to the waiting list.
      // NOTE: It's necessary to create an entry for the new SCGI conn in the fd_map; however, that will be done by the caller in this case.
    }
    // Finally, add the client connection to the waiting list for this UID. (We don't need to check if it's already there; see notes.txt for the explanation.)
    iter->second.insert(connection_fd);
    
    return -1;
  }
  else
  {
    /* NOTE: setting O_NONBLOCK is done in createSCGIConnState
    flags = fcntl(scgi_fd, F_GETFL);
    flags |= O_NONBLOCK;
    fcntl(scgi_fd, F_SETFL, flags);
    */
    return scgi_fd;
  }
}

void ProcessManager::spawnProcess(uid_t user)
{
  ostringstream user_str;
  user_str << user;
  const char *user_cstr(user_str.str().c_str());
  // string homedir("HOME=/home/" + username);
  // char *homedir_cstr(homedir.c_str());
  // char *username_cstr(username.c_str());
  
  pid_t my_pid(fork());
  if (0 == my_pid)
  {
    // This is the child process. WARNING! We must close all connection fds we inherited from the parent, so that we don't leak them to the SCGI server! (Or, use O_CLOEXEC or FD_CLOEXEC.)
    // NOTE: We will use FD_CLOEXEC.
    
    // execve(LIBEXEC_DIR "/start_server", [user_cstr, username_cstr, (char *) NULL], [homedir_cstr, (char *) NULL]);
    static char *const envp[] = {static_cast<char *>(NULL)};
    execle(LIBEXEC_DIR "/start_server", LIBEXEC_DIR "/start_server", user_cstr, static_cast<char *>(NULL), envp);
    fprintf(stderr, "Error: failed to execle the \"start_server\" suid wrapper (%s)\n", strerror(errno));
    abort();
  }
}

void ProcessManager::handleAlarm(map<int, FdState> &fd_map, int epollfd)
{
  //fprintf(stderr, "DEBUG: We got an alarm tick\n");
  
  FdState *client_state/*, *scgi_state*/;
  map<uid_t, set<int> >::iterator iter(waiting_conns.begin()), end(waiting_conns.end()), iter_del;
  set<int>::iterator set_iter, set_end, set_iter_del;
  int client_fd;
  sockaddr_un addr;
  int scgi_fd, flags;
  bool populated_addr_for_current_uid;
  
  while (end != iter)
  {
    set_iter = iter->second.begin();
    set_end = iter->second.end();
    
    populated_addr_for_current_uid = false;
    
    while (set_end != set_iter)
    {
      // Here, we retry to open a connection to the server for this user.
      client_fd = *set_iter;
      client_state = &(fd_map.find(client_fd)->second);
      // NOTE: The addr will be the same for all conns who have the same UID.
      if (!populated_addr_for_current_uid)
      {
        populateAddr(addr, client_state->username);
        populated_addr_for_current_uid = true;
      }
      scgi_fd = socket(AF_LOCAL, SOCK_STREAM, 0);
      flags = fcntl(scgi_fd, F_GETFD);
      flags |= FD_CLOEXEC;
      fcntl(scgi_fd, F_SETFD, flags);
      // NOTE: We don't set O_NONBLOCK on it until it's connected. (This is because connect(2) should be fast on an AF_LOCAL socket, and it's easier to not have to be prepared to handle EINPROGRESS error status.)
      if (-1 == attemptConnect(scgi_fd, static_cast<sockaddr *>(static_cast<void *>(&addr)), sizeof(addr)))
      {
        int saved_errno(errno);
        // We still could not connect to the SCGI server. So, we must leave this fd in the "waiting list" (the set), and proceed to the next client conn for this UID.
        fprintf(stderr, "Warning: failed a repeat attempt to connect to SCGI server at socket \"%s\" (%s)\n", string(addr.sun_path, sizeof(addr.sun_path)).c_str(), strerror(errno));
        ++set_iter;
      }
      else
      {
        // We succeeded in opening a connection to the SCGI server.
        client_state->associated_fd = scgi_fd; // Store the fd of its associated SCGI connection.
        
        size_t length_to_move;
        // So, we need to transition the client conn to its next phase.
        if (client_state->doing_post)
        {
          // Client is doing a POST request. We will proceed to either FWD_REQUEST phase or FWD_RESPONSE phase, depending on whether the entirety of the request body (the POST data) has already been read from found_fd into incoming_data or not.
          /* client_state->phase = FWD_REQUEST; */
          client_state->post_queue.clear(); // Initialize the CircBuf used for forwarding the POST data.
          
          length_to_move = minimum<size_t>(client_state->incoming_data.size() - client_state->next_chr_to_read, client_state->post_content_length);
          // NOTE: This write to post_queue will ALWAYS write the entire "length_to_move" bytes; see comment in main for the reason why.
          client_state->post_queue.write(client_state->incoming_data.substr(client_state->next_chr_to_read, length_to_move).data(), length_to_move);
          // NOTE: We must initialize post_bytes_read with the amount that we wrote to post_queue just now.
          client_state->post_bytes_read = length_to_move;
          // Now, we will cut off the initial portion of incoming_data which has alredy been read.
          client_state->incoming_data.erase(0, client_state->next_chr_to_read + length_to_move);
          client_state->next_chr_to_read = 0;
          if (length_to_move != client_state->post_content_length)
          {
            // There is more of the request body that needs to be read from found_fd. So, we must transition to FWD_REQUEST phase.
            client_state->phase = FWD_REQUEST;
            
            // Watch this client conn fd *only* when post_queue is *not* full (need to check now if it's full).
            if (client_state->post_queue.maxAmount() != length_to_move)
              // It's not full, so resume watching.
              epoll_ctl(epollfd, EPOLL_CTL_ADD, client_fd, &(client_state->event));
          }
        }
        // We will transition to FWD_RESPONSE_HDR phase under either of two conditions:
        // 1) This is a GET request; or
        // 2) This is a POST request, and the entirety of the request body (the POST data) has already been read from found_fd and placed into post_queue.
        if ((!(client_state->doing_post)) || (length_to_move == client_state->post_content_length))
        {
          // Client is doing a GET request, so we need to transition the client conn to FWD_RESPONSE_HDR phase.
          client_state->phase = FWD_RESPONSE_HDR;
          client_state->response_queue.clear(); // Initialize the CircBuf used for forwarding the response data.
          // While in FWD_RESPONSE_HDR phase, we will only be writing to the client conn fd.
          client_state->event.events = EPOLLOUT;
          // NOTE: In this phase, we don't watch the fd until our associated SCGI conn has prepared the "linear queue" response_hdr_to_send, at which point it adds our fd back to the watched set. So, we don't resume watching the fd here.
        }
        
        // Then, we must create an entry for the new SCGI conn in the fd_map.
        createSCGIConnState(scgi_fd, client_fd, fd_map, epollfd);
        
        // We must remove the client conn fd from the "waiting list" (the set) for this UID.
        set_iter_del = set_iter;
        ++set_iter; // Proceed to next client conn for this UID.
        iter->second.erase(set_iter_del);
      }
    }
    // We concluded processing the client fds for one UID. We must check if the set for this UID is now empty; if so, then we need to delete this UID from "waiting_conns".
    if (iter->second.empty())
    {
      iter_del = iter;
      ++iter;
      waiting_conns.erase(iter_del);
    }
    else
      ++iter;
  }
  // Finally, we need to check if any UIDs remain in "waiting_conns"; if not, then we need to stop the alarm.
  if (waiting_conns.empty())
    alarm_running = false;
}

void createSCGIConnState(int scgi_fd, int client_fd, map<int, FdState> &fd_map, int epollfd)
{
  //fcntl(scgi_fd, F_SETFL, O_NONBLOCK);
  int flags(fcntl(scgi_fd, F_GETFL));
  flags |= O_NONBLOCK;
  fcntl(scgi_fd, F_SETFL, flags);
  FdState *scgi_state(&((fd_map.insert(pair<int, FdState>(scgi_fd, FdState(EPOLLOUT, scgi_fd, SCGI_REQUEST_VARS)))).first->second)); // It begins in SCGI_REQUEST_VARS phase, and fd is watched for able-to-output status.
  epoll_ctl(epollfd, EPOLL_CTL_ADD, scgi_fd, &(scgi_state->event));
  scgi_state->associated_fd = client_fd;
}

int attemptConnect(int sockfd, const sockaddr *addr, socklen_t addrlen)
{
  int retval;
  
  do
  {
    retval = connect(sockfd, addr, addrlen);
  } while ((-1 == retval) && (EINTR == errno));
  
  return retval;
}

int attemptClose(int fd)
{
  int retval;
  
  do
  {
    retval = close(fd);
  } while ((-1 == retval) && (EINTR == errno));
  
  return retval;
}

int attemptAccept(int sockfd, sockaddr *addr, socklen_t *addrlen)
{
  int retval;
  
  do
  {
    retval = accept(sockfd, addr, addrlen);
  } while ((-1 == retval) && (EINTR == errno));
  
  return retval;
}

ssize_t attemptSend(int sockfd, const void *buf, size_t len, int flags)
{
  ssize_t sent_size;
  
  do
  {
    sent_size = send(sockfd, buf, len, flags);
  } while ((-1 == sent_size) && (EINTR == errno));
  
  if (-1 == sent_size)
  {
    if ((EPIPE == errno) || (ECONNRESET == errno) || (ETIMEDOUT == errno) || (ENOTCONN == errno))
      // It appears that the connection is broken.
      return -1;
    else
    {
      // Other errors should not happen. If one does, it's a bug!
      // fprintf(stderr, "Error: error %d (%s) occurred while attempting to send. This should never happen.\n", errno, strerror(errno));
      // abort();
      // NOTE: I decided to treat unknown errors as disconnects; see below.
      fprintf(stderr, "Warning: error %d (%s) occurred while attempting to send. This should never happen.\n", errno, strerror(errno));
      attemptClose(sockfd);
      return -1;
     }
  }
  
  return sent_size;
}

ssize_t attemptRecv(int sockfd, void *buf, size_t len, int flags)
{
  ssize_t recvd_size;
  
  do
  {
    recvd_size = recv(sockfd, buf, len, flags);
  } while ((-1 == recvd_size) && (EINTR == errno));
  
  if (1 > recvd_size)
  {
    if ((0 == recvd_size) || (EPIPE == errno) || (ECONNRESET == errno) || (ETIMEDOUT == errno) || (ENOTCONN == errno))
      // It appears that the connection is closed.
      return -1;
    else
    {
      // Other errors should not happen. If one does, it's a bug!
      // fprintf(stderr, "Error: error %d (%s) occurred while attempting to recv. This should never happen.\n", errno, strerror(errno));
      // abort();
      // WARNING! For some reason, I got errno 95 (EOPNOTSUPP, Operation not supported) here. Let's see if I can get away with treating unknown errors same as the known "disconnect" errors (except for printing a warning).
      fprintf(stderr, "Warning: error %d (%s) occurred while attempting to recv. This should never happen.\n", errno, strerror(errno));
      attemptClose(sockfd);
      return -1;
    }
  }
  return recvd_size;
}

int main(int argc, char **argv)
{
  // The arguments we expect are:
  // (Required ones:)
  // argv[1]: A hostname or IP address; we resolve it to an address, and then listen (on the port given in argv[2]) for incoming conns from HTTP clients on it.
  // argv[2]: The port number on which we will listen for incoming conns from HTTP clients. (Using a "service name" here might sort-of work, but don't do it!)
  // argv[3]: Pathname/filename of the authentication database file.
  // (Optional ones:)
  // argv[4]: "External" hostname or IP address, which we use in SCGI env var SERVER_NAME; if absent, we use argv[1]. (This is helpful for running the server behind a NAT gateway.)
  // argv[5]: "External" port number, which we use in SCGI env var SERVER_PORT; if absent, we use argv[2]. (This is helpful for running the server behind a NAT gateway.)
  // argv[6]: A string indicating whether the server is running behind an SSL proxy. If it is behind an SSL proxy, give the string "on" or "1"; otherwise, give any other string. (This is helpful for using the program "stunnel" with the server.)
  
  if ((4 != argc) && (6 != argc) && (7 != argc))
  {
    fprintf(stderr, "Usage: %s HOST PORT DBFILE [EXTERNAL_HOST EXTERNAL_PORT] [EXTERNAL_SSL]\n", argv[0]);
    abort();
  }
  
  queue<int, list<int> > enter_parse_request_queue;
  
  // Create the TCP listening socket for incoming HTTP connections.
  int listener(socket(AF_INET, SOCK_STREAM, 0)), retval;
  //fcntl(tcp_listener, F_SETFL, O_NONBLOCK);
  int flags(fcntl(listener, F_GETFD));
  flags |= FD_CLOEXEC;
  fcntl(listener, F_SETFD, flags);
  flags = fcntl(listener, F_GETFL);
  flags |= O_NONBLOCK;
  fcntl(listener, F_SETFL, flags);
  addrinfo *found_addr, hints({0, AF_INET, SOCK_STREAM, 0, 0, NULL, NULL, NULL});
  retval = getaddrinfo(argv[1], argv[2], &hints, &found_addr);
  if (0 != retval)
  {
    fprintf(stderr, "Error: could not resolve host %s port %s (%s)\n", argv[1], argv[2], gai_strerror(retval));
    exit(2);
  }
  retval = bind(listener, found_addr->ai_addr, found_addr->ai_addrlen);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not bind to address %s port %s (%s)\n", argv[1], argv[2], strerror(errno));
    exit(3);
  }
  retval = listen(listener, SOMAXCONN);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not listen on address %s port %s (%s)\n", argv[1], argv[2], strerror(errno));
    exit(4);
  }
  
  char *listen_host, *listen_port;
  if (6 <= argc)
  {
    listen_host = argv[4];
    listen_port = argv[5];
  }
  else
  {
    listen_host = argv[1];
    listen_port = argv[2];
  }
  
  const char *https_indicator((7 == argc) ? argv[6] : "off");
  
  // Create the datagram Unix socket for user management messages.
  int um_fd(socket(AF_LOCAL, SOCK_DGRAM, 0));
  flags = fcntl(um_fd, F_GETFD);
  flags |= FD_CLOEXEC;
  fcntl(um_fd, F_SETFD, flags);
  flags = fcntl(um_fd, F_GETFL);
  flags |= O_NONBLOCK;
  fcntl(um_fd, F_SETFL, flags);
  sockaddr_un um_addr;
  um_addr.sun_family = AF_UNIX;
  string um_path(WEB_ROOT_DIR UM_COMM_DIR_SUFFIX "/um_socket");
  if (sizeof(sun_sizecheck.sun_path) < um_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", um_path.c_str(), sizeof(sun_sizecheck.sun_path));
    abort();
  }
  retval = unlink(um_path.c_str());
  if ((-1 == retval) && (ENOENT != errno))
  {
    fprintf(stderr, "Error: file \"%s\" already exists and could not delete it (%s)\n", um_path.c_str(), strerror(errno));
    abort();
  }
  strncpy(um_addr.sun_path, um_path.data(), sizeof(sun_sizecheck.sun_path));
  retval = bind(um_fd, static_cast<sockaddr *>(static_cast<void *>(&um_addr)), sizeof(um_addr));
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not bind to pathname \"%s\" (%s)\n", um_path.c_str(), strerror(errno));
    abort();
  }
  
  // Open the user authentication database.
  u_int32_t db_env_flags(DB_CREATE     |
                         DB_INIT_LOCK  |
                         DB_INIT_LOG   |
                         DB_INIT_MPOOL |
                         DB_INIT_TXN);
  u_int32_t db_flags(DB_CREATE | DB_AUTO_COMMIT);
  // static const char *file_name = "mydb.db";
  DbEnv auth_db_env(0);
  // NOTE: The first parameter to the environment's "open" is the pathname to a "home" directory. It can also be set using the DB_HOME env var. For now, I'm not going to specify a home dir.
  auth_db_env.open(NULL, db_env_flags, 0);
  Db *auth_db(new Db(&auth_db_env, 0));
  auth_db->open(NULL, argv[3], NULL, DB_HASH, db_flags, 0);
  // WARNING! We need to ensure that FD_CLOEXEC is set for the db's file descriptor, just like all other fds (except stdin, stdout, and stderr). We do that here.
  int auth_db_fd(-1);
  auth_db->fd(&auth_db_fd);
  flags = fcntl(auth_db_fd, F_GETFD);
  flags |= FD_CLOEXEC;
  fcntl(auth_db_fd, F_SETFD, flags);
  
  // Create the epoll fd for monitoring fd activity.
  int epollfd(epoll_create1(0));
  if (-1 == epollfd)
  {
    fprintf(stderr, "Error: could not create epollfd (%s)\n", strerror(errno));
    exit(4);
  }
  flags = fcntl(epollfd, F_GETFD);
  flags |= FD_CLOEXEC;
  fcntl(epollfd, F_SETFD, flags);
  
  // Create a signal mask which does *not* have SIGALRM, SIGINT, SIGTERM, and SIGCHLD blocked.
  sigset_t trapped_signals_sigset;
  sigprocmask(SIG_UNBLOCK, NULL, &trapped_signals_sigset);
  sigdelset(&trapped_signals_sigset, SIGALRM);
  sigdelset(&trapped_signals_sigset, SIGINT);
  sigdelset(&trapped_signals_sigset, SIGTERM);
  sigdelset(&trapped_signals_sigset, SIGCHLD);
  
  // Ensure that the process's normal signal mask has SIGALRM, SIGINT, SIGTERM, and SIGCHLD blocked.
  sigset_t modification_sigset;
  sigemptyset(&modification_sigset);
  sigaddset(&modification_sigset, SIGALRM);
  sigaddset(&modification_sigset, SIGINT);
  sigaddset(&modification_sigset, SIGTERM);
  sigaddset(&modification_sigset, SIGCHLD);
  sigprocmask(SIG_BLOCK, &modification_sigset, NULL);
  
  // Register signal handlers for SIGALRM, SIGINT, SIGTERM, and SIGCHLD.
  struct sigaction modification_sigaction;
  sigprocmask(SIG_BLOCK, NULL, &(modification_sigaction.sa_mask));
  modification_sigaction.sa_handler = &setAlarmHappenedFlag;
  modification_sigaction.sa_flags = 0;
  sigaction(SIGALRM, &modification_sigaction, NULL);
  modification_sigaction.sa_handler = &setInterruptHappenedFlag;
  sigaction(SIGINT, &modification_sigaction, NULL);
  modification_sigaction.sa_handler = &setTerminateHappenedFlag;
  sigaction(SIGTERM, &modification_sigaction, NULL);
  modification_sigaction.sa_handler = &setSubprocessExitedFlag;
  sigaction(SIGCHLD, &modification_sigaction, NULL);
  
  // Ignore SIGPIPE.
  modification_sigaction.sa_handler = SIG_IGN;
  sigaction(SIGPIPE, &modification_sigaction, NULL);

  alarm_happened = false;
  alarm_running = false;
  
  interrupt_happened = false;
  terminate_happened = false;
  sigchld_happened = false;
  
  map<int, FdState> fd_map;
  FdState *state;
  
  // Enable watching of the TCP listener fd and the user management fd.
  state = &((fd_map.insert(pair<int, FdState>(listener, FdState(EPOLLIN, listener, LISTEN)))).first->second);
  epoll_ctl(epollfd, EPOLL_CTL_ADD, listener, &(state->event));
  state = &((fd_map.insert(pair<int, FdState>(um_fd, FdState(EPOLLIN, um_fd, USER_MANAGER)))).first->second);
  epoll_ctl(epollfd, EPOLL_CTL_ADD, um_fd, &(state->event));
  
  const int MAX_EVENTS(10);
  epoll_event found_events[MAX_EVENTS];
  int num_found, i, new_fd, found_fd;
  char temp_buf[BUF_SIZE], temp_buf_2[BUF_SIZE];
  ssize_t recvd_amount;
  size_t remaining_bytes, bytes_to_read;
  
  regex_t req_line_pat, hdr_line_pat, auth_val_pat, basic_val_pat, trenc_val_pat;
  regcomp(&req_line_pat, "^([^[:space:]]+)[[:space:]]+([^[:space:]]+)[[:space:]]+([^[:space:]]+)[[:space:]]*$", REG_EXTENDED);
  regcomp(&hdr_line_pat, "^([^[:space:]]+)[[:space:]]*:[[:space:]]*(.*[^[:space:]]|)[[:space:]]*$", REG_EXTENDED);
  regcomp(&auth_val_pat, "^([^[:space:]]+)[[:space:]]+([^[:space:]]+)$", REG_EXTENDED);
  regcomp(&basic_val_pat, "^([^:]*):(.*)$", REG_EXTENDED);
  regcomp(&trenc_val_pat, "^(.*[[:space:]]|)([^[:space:]]*)$", REG_EXTENDED);
  regmatch_t matches[10];
  // string username;
  // istringstream to_number;
  // map<string, string> auth_map;
  
  passwd *pw_entry;
  group *gr_entry(getgrnam("projusers"));
  if (NULL == gr_entry)
  {
    fprintf(stderr, "Error: could not find group \"projusers\" (%s)\n", strerror(errno));
    exit(5);
  }
  const gid_t projusers_gid(gr_entry->gr_gid);
  
  int ready_count;
  socklen_t ca_len;
  ProcessManager proc_mgr;
  
  // Temp vars used by USER_MANAGER phase:
  u_int32_t username_len, password_len;
  DbTxn *txn;
  Dbt key, value;
  Dbc *cursorp;
  
  // Temp vars used by PARSE_REQUEST phase:
  bool data_is_avail;
  
  // Temp vars used by WAIT_FOR_SCGI phase:
  size_t initial_amount;
  FdState *assoc_state;
  
  // Temp vars used by FWD_RESPONSE_BODY phase:
  size_t bytes_to_move;
  ssize_t bytes_sent;
  
  // Temp vars used by SCGI_RESPONSE_HDR phase:
  size_t separator_pos;
  
  for (;;)
  {
    ready_count = epoll_pwait(epollfd, found_events, MAX_EVENTS, -1, &trapped_signals_sigset);
    if (-1 == ready_count)
    {
      if (EINTR != errno)
      {
        fprintf(stderr, "Error: error in epoll_pwait (%s)\n", strerror(errno));
        exit(5);
      }
      else
      {
        // The epoll_pwait was interrupted. Check which signal interrupted it.
        if (alarm_happened)
        {
          // The signal was SIGALRM.
          // Because of real-time timing unpredictability, we *might* get a SIGALRM when the alarm is not suppored to be running. If that happens, we must not call the alarm handler. (Actually, I'm not sure that that's true, but if it isn't, it's still ok to have this code.)
          if (alarm_running)
            proc_mgr.handleAlarm(fd_map, epollfd);
          
          // Now, after handling the alarm, we check if the alarm is still supposed to be running; if so, then schedule the next SIGALRM.
          if (alarm_running)
            alarm(1);
          
          // Reset the flag after we're done.
          alarm_happened = false;
        }
        
        if (sigchld_happened)
        {
          // Reset the flag.
          sigchld_happened = false;
          
          int status;
          waitpid(-1, &status, WNOHANG);
          // TODO: Maybe print a message here?
        }
        
        if (interrupt_happened || terminate_happened)
        {
          // The signal was SIGINT or SIGTERM, so quit the program.
          break;
        }
        
        
        continue;
      }
    }
    
    // The call to epoll_pwait succeeded. Iterate through the fd events and handle them.
    for (i = 0; i < ready_count; ++i)
    {
      found_fd = found_events[i].data.fd;
      state = &(fd_map.find(found_fd)->second);
      switch (state->phase)
      {
        case LISTEN:
          // Open new client connection
          ca_len = sizeof(state->client_addr);
          new_fd = attemptAccept(listener, static_cast<sockaddr *>(static_cast<void *>(&(state->client_addr))), &ca_len);
          flags = fcntl(new_fd, F_GETFD);
          flags |= FD_CLOEXEC;
          fcntl(new_fd, F_SETFD, flags);
          flags = fcntl(new_fd, F_GETFL);
          flags |= O_NONBLOCK;
          fcntl(new_fd, F_SETFL, flags);
          state = &((fd_map.insert(pair<int, FdState>(new_fd, FdState(EPOLLIN, new_fd, PARSE_REQUEST)))).first->second);
          epoll_ctl(epollfd, EPOLL_CTL_ADD, new_fd, &(state->event));
          break;
          
        case USER_MANAGER:
          // TODO: resume here!
          attemptRecv(found_fd, temp_buf, BUF_SIZE, 0);
          txn = NULL;
          switch (*(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + ACTION_CODE_OFFSET))
          {
            case u_int32_t(ADD_USER):
              auth_db_env.txn_begin(NULL, &txn, 0);
              key.set_data(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET));
              username_len = *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET);
              key.set_flags(DB_DBT_USERMEM);
              key.set_size(username_len);
              key.set_ulen(username_len);
              value.set_data(static_cast<void *>(static_cast<char *>(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET)) + username_len));
              password_len = *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + PASSWORD_LEN_OFFSET);
              value.set_flags(DB_DBT_USERMEM);
              value.set_size(password_len);
              value.set_ulen(password_len);
              retval = auth_db->put(txn, &key, &value, DB_NOOVERWRITE);
              if (DB_KEYEXIST == retval)
                fprintf(stderr, "Error: cannot create user \"%s\" because user already exists\n", string(static_cast<char *>(key.get_data()), username_len).c_str());
              // TODO: implement sending of a "reply" message with error/succes status (if time)
              txn->commit(0);
              break;
              
            case u_int32_t(DELETE_USER):
              auth_db_env.txn_begin(NULL, &txn, 0);
              key.set_data(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET));
              username_len = *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET);
              key.set_flags(DB_DBT_USERMEM);
              key.set_size(username_len);
              key.set_ulen(username_len);
              auth_db->del(txn, &key, 0);
              txn->commit(0);
              break;
              
            case u_int32_t(CHANGE_PASSWORD):
              auth_db_env.txn_begin(NULL, &txn, 0);
              auth_db->cursor(txn, &cursorp, 0);
              key.set_data(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET));
              username_len = *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET);
              key.set_flags(DB_DBT_USERMEM);
              key.set_size(username_len);
              key.set_ulen(username_len);
              value.set_data(static_cast<void *>(temp_buf_2));
              value.set_flags(DB_DBT_USERMEM);
              value.set_size(BUF_SIZE);
              value.set_ulen(BUF_SIZE);
              retval = cursorp->get(&key, &value, DB_SET);
              if (0 != retval)
              {
                fprintf(stderr, "Error: cannot change password for user \"%s\" because user does not exist\n", string(static_cast<char *>(key.get_data()), username_len).c_str());
              }
              else
              {
                value.set_data(static_cast<void *>(static_cast<char *>(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET)) + username_len));
                value.set_size(*(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + PASSWORD_LEN_OFFSET));
                //value.set_flags(DB_DBT_USERMEM);
                cursorp->put(&key, &value, DB_CURRENT);
              }
              txn->commit(0);
              break;
              
            default: // invalid action code
              break;
          }
          break;
          
        case PARSE_REQUEST:
          data_is_avail = false;
          // NOTE: possibly this if-else should be made into a procedure
          if (state->need_to_recv)
          {
            recvd_amount = attemptRecv(found_fd, temp_buf, BUF_SIZE, 0);
            if (-1 == recvd_amount) // -1 means connection was closed by other end
            {
                // Delete (all that remains of) this connection.
                epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
                attemptClose(found_fd);
                fd_map.erase(found_fd);
            }
            else
            {
              state->incoming_data.append(temp_buf, recvd_amount);
              data_is_avail = true;
            }
          }
          else
            data_is_avail = true;
          
          if (data_is_avail)
            handleReceivedHTTPRequestFragment(epollfd, fd_map, proc_mgr, state, found_fd, projusers_gid, req_line_pat, hdr_line_pat, auth_val_pat, basic_val_pat, matches, auth_db_env, auth_db, key, value, temp_buf, temp_buf_2, listen_host, listen_port, https_indicator);
          
          break;
          
        case ERROR_CLOSE:
        case ERROR_NOCLOSE:
          remaining_bytes = (state->response_text.size() - state->error_response_bytes_written);
          
          bytes_sent = attemptSend(found_fd, state->response_text.substr(state->error_response_bytes_written).data(), remaining_bytes, 0);
          if (-1 == bytes_sent) // -1 means connection was closed by other end
          {
            // Delete (all that remains of) this connection.
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            state->error_response_bytes_written += bytes_sent;
            if (state->response_text.size() == state->error_response_bytes_written)
            {
              // We are done sending the error response.
              switch (state->phase)
              {
                case ERROR_CLOSE:
                  // Delete this client connection.
                  epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
                  attemptClose(found_fd);
                  fd_map.erase(found_fd);
                  break;
                  
                default: // case here is ERROR_NOCLOSE
                  // Return this connection to PARSE_REQUEST phase.
                  state->phase = PARSE_REQUEST;
                  state->event.events = EPOLLIN;
                  epoll_ctl(epollfd, EPOLL_CTL_MOD, found_fd, &(state->event));
                  // When a connection enters PARSE_REQUEST phase, add it to a queue, then run through the queue as the last thing in body of event loop. (Purpose is to handle multiple HTTP requests that arrived in a single recv.)
                  enter_parse_request_queue.push(found_fd);
                  break;
              }
            }
          }
          break;
          
        case WAIT_FOR_SCGI:
          // NOTE: We should never actually get here, because connections in WAIT_FOR_SCGI phase shouldn't be monitored by epoll.
          break;
          
        case FWD_REQUEST:
          // Here, we must move data from found_fd into state->post_queue.
          //size_t initial_amount(state->post_queue.amount());
          initial_amount = state->post_queue.amount();
          // The maximum amount of bytes to read is either the available space in post_queue or the size of the remaining portion of request body, whichever is less.
          bytes_to_read = minimum<size_t>(state->post_queue.maxAmount() - initial_amount, state->post_content_length - state->post_bytes_read);
          // Read at most bytes_to_read bytes from found_fd.
          recvd_amount = attemptRecv(found_fd, temp_buf, bytes_to_read, 0);
          if (-1 == recvd_amount) // -1 means connection was closed by other end
          {
            // fprintf(stderr, "DEBUG: in FWD_REQUEST, client conn closed unexpectedy, so we delete both client conn and SCGI conn\n");
            
            // We need to delete both this conn (to client) and the associated_fd conn (to SCGI server).
            assoc_state = &(fd_map.find(state->associated_fd)->second);
            epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
            attemptClose(state->associated_fd);
            fd_map.erase(state->associated_fd);
            
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            // fprintf(stderr, "DEBUG: in FWD_REQUEST, post_queue had %llu bytes free and we wrote %llu bytes into it\n", bytes_to_read, recvd_amount);
            // fprintf(stderr, "DEBUG: in FWD_REQUEST, we read %llu of %llu bytes from post_queue\n", bytes_sent, bytes_to_move);
            
            // Put all the read bytes into post_queue (we know there's enough space).
            state->post_queue.write(temp_buf, recvd_amount);
            // Disable watching of found_fd for input *iff* post_queue is now full.
            if ((initial_amount + recvd_amount) == state->post_queue.maxAmount())
            {
              // fprintf(stderr, "DEBUG: in FWD_REQUEST, we went to sleep (because post_queue is full)\n");
              epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            }
            // Enable watching of state->associated_fd *iff* (it's in SCGI_REQUEST_BODY phase AND post_queue was empty before write AND at least 1 byte was written to post_queue)
            if ((0 == initial_amount) && (0 != recvd_amount))
            {
              assoc_state = &(fd_map.find(state->associated_fd)->second);
              if (SCGI_REQUEST_BODY == assoc_state->phase)
              {
                // fprintf(stderr, "DEBUG: in FWD_REQUEST, we woke up the SCGI conn (because post_queue was empty but now it is not)\n");
                epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
              }
            }
            
            state->post_bytes_read += recvd_amount;
            // fprintf(stderr, "DEBUG: in FWD_REQUEST, post_bytes_read is %llu and post_content_length is %llu\n", state->post_bytes_read, state->post_content_length);
            if (state->post_bytes_read == state->post_content_length)
            {
              // fprintf(stderr, "DEBUG: in FWD_REQUEST, we transition to FWD_RESPONSE_HDR (because we have now written all %llu bytes)\n", state->post_content_length);
              
              // We've concluded reading the request body out of found_fd. Now, we must transition to FWD_RESPONSE_HDR phase.
              state->phase = FWD_RESPONSE_HDR;
              state->response_queue.clear();
              // In FWD_RESPONSE phase, we want to watch the fd for able-to-write status.
              state->event.events = EPOLLOUT;
              // // However, we will have watching turned off when response_queue is empty, and it is empty to begin with.
              // However, we will have watching turned off until our associated SCGI conn has prepared the "linear queue" response_hdr_to_send, at which point it adds our fd back to the watched set. So, suspend watching the fd here.
              epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            }
          }
          break;
          
        case FWD_RESPONSE_HDR:
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          // if (state->response_hdr_to_send.size() != state->response_hdr_bytes_written)
          // {
          // // NOTE: Can we get rid of the above if? It seems like it should always be true.
          remaining_bytes = (state->response_hdr_to_send.size() - state->response_hdr_bytes_written);
          
          // fprintf(stderr, "DEBUG: in FWD_RESPONSE_HDR, we see %llu bytes (\"%s\") in response_hdr_to_send\n", remaining_bytes, state->response_hdr_to_send.c_str());
          
          bytes_sent = attemptSend(found_fd, state->response_hdr_to_send.substr(state->response_hdr_bytes_written).data(), remaining_bytes, 0);
          
          // fprintf(stderr, "DEBUG: in FWD_RESPONSE_HDR, we sent %llu bytes\n", bytes_sent);
          
          if (-1 == bytes_sent)
          {
            // We need to delete both this conn (to client) and the associated_fd conn (to SCGI server).
            epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
            attemptClose(state->associated_fd);
            fd_map.erase(state->associated_fd);
            
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            state->response_hdr_bytes_written += bytes_sent;
            
            if (remaining_bytes == bytes_sent)
            {
              if ((SCGI_ZOMBIE == assoc_state->phase) && (0 == state->response_queue.amount()))
              {
                // If we get here, that would seem to mean that the size of the response body is zero bytes. So, we need to delete what's left of the associated conn, and then transition to PARSE_REQUEST phase. (NOTE: This means we skip past the FWD_RESPONSE_BODY phase in this case.)
                attemptClose(state->associated_fd);
                fd_map.erase(state->associated_fd);
                
                state->phase = PARSE_REQUEST;
                state->event.events = EPOLLIN;
                epoll_ctl(epollfd, EPOLL_CTL_MOD, found_fd, &(state->event));
                // Put found_fd into the queue of conns that have transitioned to PARSE_REQUEST phase (see notes for why this is necessary)
                enter_parse_request_queue.push(found_fd);
              }
              else
              {
                // We need to transition to FWD_RESPONSE_BODY phase.
                state->phase = FWD_RESPONSE_BODY;
                if (0 == state->response_queue.amount())
                {
                  // We're out of bytes to write, so disable watching found_fd for able-to-write (until there are more bytes to write).
                  epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
                }
              }
            }
          }
          break;
          
        case FWD_RESPONSE_BODY:
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          bytes_to_move = state->response_queue.read(temp_buf, BUF_SIZE, NO_MODIFY);
          
          bytes_sent = attemptSend(found_fd, temp_buf, bytes_to_move, 0);
          
          // fprintf(stderr, "DEBUG: in FWD_RESPONSE_BODY, we read %llu of %llu bytes from response_queue\n", bytes_sent, bytes_to_move);
          
          if (-1 == bytes_sent)
          {
            // fprintf(stderr, "DEBUG: in FWD_RESPONSE_BODY, client conn closed unexpectedy, so we delete both client conn and SCGI conn\n");
            
            // We need to delete both this conn (to client) and the associated_fd conn (to SCGI server).
            epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
            attemptClose(state->associated_fd);
            fd_map.erase(state->associated_fd);
            
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            // Dequeue "bytes_sent" amount of bytes from post_queue in NO_COPY mode.
            state->response_queue.read(NULL, bytes_sent, NO_COPY);
            
            if (bytes_to_move == bytes_sent)
            {
              if (SCGI_ZOMBIE == assoc_state->phase)
              {
                // fprintf(stderr, "DEBUG: in FWD_RESPONSE_BODY, we transition to PARSE_REQUEST (because response_queue is empty and SCGI conn is in SCGI_ZOMBIE)\n");
                
                /* response_finished = true; */
                // NOTE: I don't remember what that "response_finished" was supposed to be for; it doesn't show up elsewhere in the program.
                
                // Our response_queue is empty and our associated SCGI connection has finished. This means that we must delete the entry for the associated conn (which is already closed), and then we must transition to PARSE_REQUEST phase.
                
                // // associated_fd is already closed, so we just need to delete from fd_map.
                // Yes, it's closed from the other end, but do we need to close it here? Let's do that, just in case.
                attemptClose(state->associated_fd);
                fd_map.erase(state->associated_fd);
                
                state->phase = PARSE_REQUEST;
                state->event.events = EPOLLIN;
                epoll_ctl(epollfd, EPOLL_CTL_MOD, found_fd, &(state->event));
                // Put found_fd into the queue of conns that have transitioned to PARSE_REQUEST phase (see notes for why this is necessary)
                enter_parse_request_queue.push(found_fd);
              }
              else
              {
                // fprintf(stderr, "DEBUG: in FWD_RESPONSE_BODY, we went to sleep (because response_queue is empty)\n");
                
                // Our response_queue is empty, but we're not done sending the response. So, temporarily stop watching found_fd for able-to-write, until our associated SCGI conn puts more data into response_queue OR detects that it has been closed.
                epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
              }
            }
            
            // If we've taken at least 1 byte from response_queue when response_queue was full, and our associated SCGI conn is in SCGI_RESPONSE_BODY phase, then we must resume watching for data on associated SCGI conn's fd.
            // NOTE: Maybe we can omit check for SCGI_RESPONSE_BODY phase, as it can only be either that or SCGI_ZOMBIE and we already checked for the latter.
            if ((0 != bytes_sent) && (SCGI_RESPONSE_BODY == assoc_state->phase) && (state->response_queue.maxAmount() == bytes_to_move))
            {
              // fprintf(stderr, "DEBUG: in FWD_RESPONSE_BODY, we woke up the SCGI conn (because response_queue was full but now is not)\n");
              epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
            }
          }
          break;
          
        case SCGI_REQUEST_VARS:
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          remaining_bytes = (assoc_state->scgi_vars_to_send.size() - assoc_state->scgi_vars_bytes_written);
          bytes_sent = attemptSend(found_fd, assoc_state->scgi_vars_to_send.substr(assoc_state->scgi_vars_bytes_written).data(), remaining_bytes, 0);
          if (-1 == bytes_sent) // -1 means connection was closed by other end
          {
            // We need to delete both this conn (to SCGI server) and the associated_fd conn (to client).
            epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
            attemptClose(state->associated_fd);
            fd_map.erase(state->associated_fd);
            
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            assoc_state->scgi_vars_bytes_written += bytes_sent;
            if (assoc_state->scgi_vars_to_send.size() == assoc_state->scgi_vars_bytes_written)
            {
              // We are done sending the SCGI request variables.
              if (assoc_state->doing_post)
              {
                // We're doing a POST request; in this case, we will transition to SCGI_REQUEST_BODY phase.
                state->phase = SCGI_REQUEST_BODY;
                // Temporarily suspend watching of this fd *iff* the post_queue we use is currently empty.
                if (0 == assoc_state->post_queue.amount())
                {
                  epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
                }
              }
              else
              {
                // We're doing a GET request; in this case, we will transition to SCGI_RESPONSE_HDR phase.
                state->phase = SCGI_RESPONSE_HDR;
                state->event.events = EPOLLIN;
                epoll_ctl(epollfd, EPOLL_CTL_MOD, found_fd, &(state->event));
                // NOTE: The response_queue we use is guaranteed to have already been initialized by the time we get here, and is also guaranteed to be empty at this point (because only a conn in SCGI_RESPONSE phase writes to response_queue).
              }
            }
          }
          break;
          
        case SCGI_REQUEST_BODY:
          // Here, we must move data from assoc_state->post_queue into found_fd.
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          bytes_to_move = assoc_state->post_queue.read(temp_buf, BUF_SIZE, NO_MODIFY);
          bytes_sent = attemptSend(found_fd, temp_buf, bytes_to_move, 0);
          
          // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, we read %llu of %llu bytes from response_queue\n", bytes_sent, bytes_to_move);
          
          if (-1 == bytes_sent)
          {
            // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, SCGI conn closed unexpectedy, so we delete both client conn and SCGI conn\n");
            
            // We need to delete both this conn (to SCGI server) and the associated_fd conn (to client).
            epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
            attemptClose(state->associated_fd);
            fd_map.erase(state->associated_fd);
            
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            attemptClose(found_fd);
            fd_map.erase(found_fd);
          }
          else
          {
            // Dequeue "bytes_sent" amount of bytes from post_queue in NO_COPY mode.
            assoc_state->post_queue.read(NULL, bytes_sent, NO_COPY);
            
            assoc_state->post_bytes_written += bytes_sent;
            // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, post_bytes_written is %llu and post_content_length is %llu\n", assoc_state->post_bytes_written, assoc_state->post_content_length);
            if (assoc_state->post_bytes_written == assoc_state->post_content_length)
            {
              // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, we transition to SCGI_RESPONSE_HDR (because we have now read all %llu bytes)\n", assoc_state->post_content_length);
              
              // We've concluded writing the request body into found_fd. Now, we must transition to SCGI_RESPONSE_HDR phase.
              state->phase = SCGI_RESPONSE_HDR;
              state->event.events = EPOLLIN;
              epoll_ctl(epollfd, EPOLL_CTL_MOD, found_fd, &(state->event));
              // NOTE: We don't need to change watch-status of state->associated_fd here, because we know that by the time we arrive here, our associated conn is in FWD_RESPONSE_HDR phase AND its watch is disabled AND we don't want to enable it until the first write into response_queue.
            }
            else
            {
              // We're not finished with SCGI_REQUEST_BODY phase yet. Check if we need to change the watch-status of found_fd and state->associated_fd.
              
              // Disable watching of found_fd *iff* post_queue is empty.
              if (0 == (bytes_to_move - bytes_sent))
              {
                // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, we went to sleep (because post_queue is empty)\n");
                epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
              }
              // Enable watching of state->associated_fd *iff* (it's in FWD_REQUEST phase AND post_queue was full before read AND post_queue is non-full now)
              if ((FWD_REQUEST == assoc_state->phase) && (assoc_state->post_queue.maxAmount() == bytes_to_move) && (0 != bytes_sent))
              {
                // fprintf(stderr, "DEBUG: in SCGI_REQUEST_BODY, we woke up the client conn (because post_queue was full but now it is not)\n");
                epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
              }
            }
          }
          break;
          
        case SCGI_RESPONSE_HDR:
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          data_is_avail = false;
          // NOTE: possibly this if-else should be made into a procedure
          if (assoc_state->scgi_need_to_recv)
          {
            recvd_amount = attemptRecv(found_fd, temp_buf, BUF_SIZE, 0);
            if (-1 == recvd_amount) // -1 means connection was closed by other end
            {
              // NOTE: If the connection is closed here, it's an error; connection shouldn't be closed until at earliest the first attemptRecv after we've got the \r\n\r\n separator.
              // We need to delete both this conn (to SCGI server) and the associated_fd conn (to client).
              epoll_ctl(epollfd, EPOLL_CTL_DEL, state->associated_fd, &(assoc_state->event));
              attemptClose(state->associated_fd);
              fd_map.erase(state->associated_fd);
              
              epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
              attemptClose(found_fd);
              fd_map.erase(found_fd);
            }
            else
            {
              assoc_state->scgi_incoming_data.append(temp_buf, recvd_amount);
              data_is_avail = true;
            }
          }
          else
            data_is_avail = true;
          
          
          if (data_is_avail)
          {
             // NOTE: line ends are \r\n, not \n
            separator_pos = assoc_state->scgi_incoming_data.find("\r\n\r\n", assoc_state->scgi_next_chr_to_read, 4);
            if (string::npos == separator_pos)
            {
              // We record the position in "incoming_data" at which we must resume scanning for the separator. This will be the third-from-last character in "incoming_data" (unless if there are less than 3 chars in "incoming_data", in which case it will be the first char).
              (assoc_state->scgi_next_chr_to_read) = assoc_state->scgi_incoming_data.size();
              if (3 > (assoc_state->scgi_next_chr_to_read))
                (assoc_state->scgi_next_chr_to_read) = 0;
              else
                (assoc_state->scgi_next_chr_to_read) -= 3;
              // NOTE: being here means that we "hit the end" of of scgi_incoming_data this time. this means we must do a "recv" to get more data next time.
              assoc_state->scgi_need_to_recv = true;
            }
            else
            {
              // NOTE: being here means that we DID NOT "hit the end" of scgi_incoming_data this time; this means we should not do a "recv" again until we've hit the end of incoming_data.
              assoc_state->scgi_need_to_recv = false;
              // We found the text of a response header set. Split it off from the incoming buffer, and then handle it.
              // state->current_request_text.assign(state->incoming_data, 0, separator_pos + 4);
              string response_hdrs_text(assoc_state->scgi_incoming_data, 0, separator_pos + 4);
              // Advance the position of incoming_data to the first byte of POST data.
              assoc_state->scgi_incoming_data.erase(0, separator_pos + 4);
              // state->next_chr_to_read = sepatator_pos + 4;
              assoc_state->scgi_next_chr_to_read = 0;
              // NOTE: the "+ 4"s above are there because:
              // - for current_request_text, we want it to conclude with *two* linebreaks (\r\n\r\n); and
              // - for incoming_data, we want it to begin with *the first byte of the POST data*, so we must skip past the \r\n\r\n separator entirely.
              
              size_t line_begin(0);
              size_t line_end_plus_one(response_hdrs_text.find("\r\n", 0, 2));
              // string line(state->current_request_text.substr(0, line_end_plus_one));
              string line;
              bool redirect(false);
              string header_name, header_val, response_tempbuf, status_text;
              
              // Prior to the loop through all headers, we initialize line_begin and line_end_plus_one to point at the beginning and end-plus-one of the *second* line of the HTTP request (the line where the first header is if there's at least one header).
              // size_t line_begin(line_end_plus_one + 2);
              // line_end_plus_one = response_hdrs_text.find("\r\n", line_begin, 2);
              
              bool need_to_close(true);
              
              for (;;)
              {
                // parsing of headers goes here
                if (line_begin == line_end_plus_one)
                  break; // End of headers detected.
                
                line.clear();
                // We execute this loop body once for each line of the *current* header. We append the text of each line to the string "line", omitting the \r\n separators. (TODO: the name of the string shouldn't be "line"; I called it that back when the program didn't support multiple lines per header.)
                do
                {
                  line.append(response_hdrs_text, line_begin, line_end_plus_one - line_begin);
                  line_begin = (line_end_plus_one + 2);
                  line_end_plus_one = response_hdrs_text.find("\r\n", line_begin, 2);
                }
                while ((' ' == response_hdrs_text[line_begin]) || ('\t' == response_hdrs_text[line_begin]));
                
                retval = regexec(&hdr_line_pat, line.c_str(), 3, matches, 0);
                if (0 == retval)
                {
                  header_name.assign(line, matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
                  header_val.assign(line, matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);
                  
                  // fprintf(stderr, "DEBUG: SCGI header name is \"%s\", header val is \"%s\"\n", header_name.c_str(), header_val.c_str());
                  
                  transform(header_name.begin(), header_name.end(), header_name.begin(), ::toupper);
                  
                  if ("STATUS" == header_name)
                  {
                    status_text.assign(header_val);
                  }
                  else
                  {
                    response_tempbuf.append(line);
                    response_tempbuf.append("\r\n");
                  }
                  
                  if ("LOCATION" == header_name)
                  {
                    redirect = true;
                  }
                  
                  if ("CONTENT-LENGTH" == header_name)
                  {
                    need_to_close = false;
                  }
                  else if ("TRANSFER-ENCODING" == header_name)
                  {
                    retval = regexec(&trenc_val_pat, header_val.c_str(), 3, matches, 0);
                    if (0 == retval)
                    {
                      string last_trenc(header_val, matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);
                      transform(last_trenc.begin(), last_trenc.end(), last_trenc.begin(), ::toupper);
                      // If "chunked" transfer-encoding is used, it must be the last one.
                      if ("CHUNKED" == last_trenc)
                      {
                        need_to_close = false;
                      }
                    }
                  }
                }
                else
                {
                  response_tempbuf.append(line);
                  response_tempbuf.append("\r\n");
                }
              }
              
              assoc_state->close_on_finish = need_to_close;
              
              // If status wasn't explicitly set, set it.
              if (0 == status_text.size())
              {
                if (redirect)
                  status_text.assign("302 Found");
                else
                  status_text.assign("200 OK");
              }
              
              // Pre-assemble the HTTP response headers in a "linear queue".
              assoc_state->response_hdr_to_send.assign("HTTP/1.1 ");
              assoc_state->response_hdr_to_send.append(status_text);
              assoc_state->response_hdr_to_send.append("\r\n");
              assoc_state->response_hdr_to_send.append(response_tempbuf);
              assoc_state->response_hdr_to_send.append("\r\n");
              assoc_state->response_hdr_bytes_written = 0;
              
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_HDR, response_hdr_to_send is \"%s\", remaining scgi_incoming_data is \"%s\"\n", assoc_state->response_hdr_to_send.c_str(), assoc_state->scgi_incoming_data.c_str());
              
              size_t temp_amount(assoc_state->response_queue.amount());
              assoc_state->response_queue.read(temp_buf, temp_amount, NO_MODIFY);
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_HDR, initially response_queue's amount() is %llu and contains \"%s\"\n", temp_amount, string(temp_buf, temp_amount).c_str());
              
              // Put the remaining contents of incoming_data into the response_queue of our associated conn.
              // NOTE: response_queue will have already been initialized by associated conn before we get here (it does this upon transition to FWD_RESPONSE phase).
              // NOTE: There will always be room for the rest of scgi_incoming_data in the response_queue, because the remaining amount in scgi_incoming_data is at most 1 byte less than BUF_SIZE (because it's all of the data returned by the last attemptRecv except for the part up to and including the \r\n\r\n separator, and the last attemptRecv must have read at least the last \n byte of \r\n\r\n), and the max size of response_queue is exactly (BUF_SIZE - 1).
              assoc_state->response_queue.write(assoc_state->scgi_incoming_data.data(), assoc_state->scgi_incoming_data.size());
              
              temp_amount = assoc_state->response_queue.amount();
              // assoc_state->response_queue.setTrace(true);
              // assoc_state->response_queue.read(temp_buf, temp_amount, NO_MODIFY);
              // assoc_state->response_queue.setTrace(false);
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_HDR, after write, response_queue's amount() is %llu and contains \"%s\"\n", temp_amount, string(temp_buf, temp_amount).c_str());
              
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_HDR, response_queue's amount() is %llu\n", assoc_state->response_queue.amount());
              
              // Transition to SCGI_RESPONSE_BODY phase.
              state->phase = SCGI_RESPONSE_BODY;
              // If response_queue is full, suspend watching this found_fd (until it is non-full again)
              if (assoc_state->response_queue.maxAmount() == assoc_state->scgi_incoming_data.size())
              {
                epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
              }
              
              // Now that there is data available for our associated conn to send, resume watching its fd for able-to-write status.
              epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
            }
          }
          break;
          
        case SCGI_RESPONSE_BODY:
          assoc_state = &(fd_map.find(state->associated_fd)->second);
          // Here, we must move data from found_fd into assoc_state->response_queue.
          initial_amount = assoc_state->response_queue.amount();
          // The maximum amount of bytes to read is the available space in response_queue.
          bytes_to_read = (assoc_state->response_queue.maxAmount() - initial_amount);
          // Read at most bytes_to_read bytes from found_fd.
          recvd_amount = attemptRecv(found_fd, temp_buf, bytes_to_read, 0);
          if (-1 == recvd_amount) // -1 means connection was closed by other end
          {
            // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_BODY, transitioning to SCGI_ZOMBIE\n");
            
            // The SCGI server has indicated the end of the response by closing its connection. We must transition into SCGI_ZOMBIE phase and wait for our associated conn to delete us.
            state->phase = SCGI_ZOMBIE;
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            
            // If our associated conn is in FWD_RESPONSE_BODY phase and is not being watched for able-to-write, we must make it resume watching for able-to-write (yes, even though we didn't write anything to response_queue), so that it can delete us and then continue on to handling another HTTP request.
            if ((FWD_RESPONSE_BODY == assoc_state->phase) && (0 == assoc_state->response_queue.amount()))
            {
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_BODY, we woke up the client conn (because response_queue is empty and SCGI conn is finished)\n");
              epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
            }
          }
          else
          {
            // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_BODY, response_queue had %llu bytes free and we wrote %llu bytes into it\n", bytes_to_read, recvd_amount);
            
             // Put all the read bytes into response_queue (we know there's enough space).
            assoc_state->response_queue.write(temp_buf, recvd_amount);
            // Disable watching of found_fd for input *iff* response_queue is now full.
            if ((initial_amount + recvd_amount) == assoc_state->response_queue.maxAmount())
            {
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_BODY, we went to sleep (because response_queue is full)\n");
              
              epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
            }
            // Enable watching of state->associated_fd *iff* (it's in FWD_RESPONSE_BODY phase AND post_queue was empty before write AND at least 1 byte was written to post_queue)
            // fprintf(stderr, "DEBUG: initial_amount is %lld, recvd_amount is %lld, assoc_state->phase is %d (FWD_RESPONSE_BODY is %d)\n", initial_amount, recvd_amount, assoc_state->phase, FWD_RESPONSE_BODY);
            if ((0 == initial_amount) && (0 != recvd_amount) && (FWD_RESPONSE_BODY == assoc_state->phase))
            {
              // fprintf(stderr, "DEBUG: in SCGI_RESPONSE_BODY, we woke up the client conn (because response_queue was empty but now it's not)\n");
              
              epoll_ctl(epollfd, EPOLL_CTL_ADD, state->associated_fd, &(assoc_state->event));
            }
          }
          break;
          
        default: // case here is SCGI_ZOMBIE
          // NOTE: We should never actually get here, because connections in SCGI_ZOMBIE phase should have already been closed and thus not monitored by epoll.
          break;
      }
    }
    
    // For any client conn that entered the PARSE_REQUEST phase during this round of fd events, we must check whether that conn has any bytes remaining in its "linear queue" incoming_data. If so, we must scan its incoming_data for the "end-of-header" marker (\r\n\r\n). (The reason for this is that it's possible, albeit unlikely, that the read from client fd which read in the \r\n\r\n marker of the HTTP request that was handled previously also read in one or more whole HTTP requests and that those one or more whole HTTP requests are *all* the rest of the requests which the client is going to send us, at least until it gets responses to those HTTP requests. In this case, it's not sufficient to watch for able-to-read on the client conn fd, because the client isn't going to write anything more; in other words, we are holding HTTP request(s) in our incoming_data but we will not receive any more able-to-read fd events either until we answer those requests or maybe not at all (the client might close the connection after it gets its responses).
    while (!(enter_parse_request_queue.empty()))
    {
      found_fd = enter_parse_request_queue.front();
      enter_parse_request_queue.pop();
      state = &(fd_map.find(found_fd)->second);
      
      if (state->close_on_finish)
      {
        // We need to close this connection to indicate end of content (because it lacks a Content-Length and does not use chunked Transfer-Encoding).
        epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
        attemptClose(found_fd);
        fd_map.erase(found_fd);
      }
      else
      {
        // Reset the SCGI-only members of "state" here.
        state->scgi_incoming_data.clear();
        state->scgi_next_chr_to_read = 0;
        state->scgi_need_to_recv = true;
        
        if (0 != state->incoming_data.size())
          handleReceivedHTTPRequestFragment(epollfd, fd_map, proc_mgr, state, found_fd, projusers_gid, req_line_pat, hdr_line_pat, auth_val_pat, basic_val_pat, matches, auth_db_env, auth_db, key, value, temp_buf, temp_buf_2, listen_host, listen_port, https_indicator);
      }
    }
  }
  
  // Shut down the server here.
  fprintf(stderr, "projhttpd is shutting down now.\n");
  // Close the user authentication database.
  auth_db->close(0);
  auth_db_env.close(0);
  
  exit(0);
}

void handleReceivedHTTPRequestFragment(int epollfd, map<int, FdState> &fd_map, ProcessManager &proc_mgr, FdState *state, int found_fd, const gid_t projusers_gid, const regex_t &req_line_pat, const regex_t &hdr_line_pat, const regex_t &auth_val_pat, const regex_t &basic_val_pat, regmatch_t *matches, DbEnv &auth_db_env, Db *auth_db, Dbt &key, Dbt &value, char *temp_buf, char *temp_buf_2, char *listen_host, char *listen_port, const char *https_indicator)
{
  size_t separator_pos;
  bool got_auth, valid_auth, require_content_length, got_content_length;
  int retval;
  string content_length_str, auth_type, auth_val;
  base64::decoder b64dec;
  u_int32_t username_len, password_len;
  passwd *pw_entry;
  string scgi_vars_tempbuf;

  // NOTE: line ends are \r\n, not \n
  separator_pos = state->incoming_data.find("\r\n\r\n", state->next_chr_to_read, 4);
  if (string::npos == separator_pos)
  {
    // We record the position in "incoming_data" at which we must resume scanning for the separator. This will be the third-from-last character in "incoming_data" (unless if there are less than 3 chars in "incoming_data", in which case it will be the first char).
    (state->next_chr_to_read) = state->incoming_data.size();
    if (3 > (state->next_chr_to_read))
      (state->next_chr_to_read) = 0;
    else
      (state->next_chr_to_read) -= 3;
    // NOTE: being here means that we "hit the end" of of incoming_data this time. this means we must do a "recv" to get more data next time.
    state->need_to_recv = true;
  }
  else
  {
    // NOTE: being here means that we DID NOT "hit the end" of incoming_data this time; this means we should not do a "recv" again until we've hit the end of incoming_data.
    state->need_to_recv = false;
    // We found the text of a request. Split it off from the incoming buffer, and then handle it.
    state->current_request_text.assign(state->incoming_data, 0, separator_pos + 4);
    // Advance the position of incoming_data to the first byte of POST data.
    state->incoming_data.erase(0, separator_pos + 4);
    // NOTE: the "+ 4"s above are there because:
    // - for current_request_text, we want it to conclude with *two* linebreaks (\r\n\r\n); and
    // - for incoming_data, we want it to begin with *the first byte of the POST data*, so we must skip past the \r\n\r\n separator entirely.
    state->next_chr_to_read = 0;
    // parse request here
    size_t line_end_plus_one(state->current_request_text.find("\r\n", 0, 2));
    string line(state->current_request_text.substr(0, line_end_plus_one));
    retval = regexec(&req_line_pat, line.c_str(), 4, matches, 0);
    if (0 != retval)
    {
      respond400BadReq(epollfd, state);
      return;
    }
    
    string method(line.substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so));
    string uri(line.substr(matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so));
    string protocol(line.substr(matches[3].rm_so, matches[3].rm_eo - matches[3].rm_so));
    
    // We will only support methods GET and POST. If POST, we must have Content-Length:.
    // fprintf(stderr, "DEBUG: method is \"%s\", uri is \"%s\", protocol is \"%s\"\n", method.c_str(), uri.c_str(), protocol.c_str());
    if ("POST" == method)
    {
      require_content_length = true;
      got_content_length = false;
    }
    else if ("GET" == method)
      require_content_length = false;
    else
    {
      respond501MethodNotImpl(epollfd, state);
      return;
    }
    
    got_auth = false;
    valid_auth = false;
    string header_name, header_val;
    // NOTE: This needs to be a map, rather than a multimap, because it's equivalent to "environ" in WSGI (which is a plain dictionary) and is equivalent to Unix env vars (which also has only one value per key) in CGI.
    map<string, string> scgi_env;
    
    scgi_env["SERVER_PROTOCOL"] = protocol;
    
    // Prior to the loop through all headers, we initialize line_begin and line_end_plus_one to point at the beginning and end-plus-one of the *second* line of the HTTP request (the line where the first header is if there's at least one header).
    size_t line_begin(line_end_plus_one + 2);
    line_end_plus_one = state->current_request_text.find("\r\n", line_begin, 2);
    
    // We execute this loop body once for each header.
    for (;;)
    {
      // parsing of headers goes here
      if (line_begin == line_end_plus_one)
        break; // End of headers detected.
      
      // line.assign(state->current_request_text, line_begin, line_end_plus_one - line_begin);
      
      line.clear();
      // We execute this loop body once for each line of the *current* header. We append the text of each line to the string "line", omitting the \r\n separators. (TODO: the name of the string shouldn't be "line"; I called it that back when the program didn't support multiple lines per header.)
      do
      {
        line.append(state->current_request_text, line_begin, line_end_plus_one - line_begin);
        line_begin = (line_end_plus_one + 2);
        line_end_plus_one = state->current_request_text.find("\r\n", line_begin, 2);
      }
      while ((' ' == state->current_request_text[line_begin]) || ('\t' == state->current_request_text[line_begin]));
      
      retval = regexec(&hdr_line_pat, line.c_str(), 3, matches, 0);
      if (0 != retval)
      {
        respond400BadReq(epollfd, state);
        return;
      }
      
      header_name.assign(line, matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
      header_val.assign(line, matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);
      
      // Convert header name to upper case and replace hyphens with underscores.
      transform(header_name.begin(), header_name.end(), header_name.begin(), headerToEnvVar<char>);
      
      // NOTE: We pass "Content-Length" as "CONTENT_LENGTH" (not "HTTP_CONTENT_LENGTH"), we pass "Content-Type" as "CONTENT_TYPE", we don't pass "Authorization" at all, and the rest we pass with "HTTP_" prepended.
      if ("CONTENT_LENGTH" == header_name)
      {
        // NOTE: SCGI requires that CONTENT_LENGTH be the *first* header. So, we don't put it in scgi_env, but instead handle it specially.
        if (require_content_length)
        {
          content_length_str.assign(header_val);
          istringstream to_number(header_val);
          to_number >> (state->post_content_length);
          // Validate that it's an integer, by converting it from string -> integer -> string and comparing the new string to original.
          ostringstream from_number;
          from_number << (state->post_content_length);
          if (from_number.str() != content_length_str)
          {
            respond400BadReq(epollfd, state);
            return;
          }
          state->post_bytes_read = 0;
          state->post_bytes_written = 0;
          got_content_length = true;
        }
      }
      else if ("AUTHORIZATION" == header_name)
      {
        if (!got_auth)
        {
          got_auth = true;
          retval = regexec(&auth_val_pat, header_val.c_str(), 3, matches, 0);
          if (0 != retval)
          {
            respond400BadReq(epollfd, state);
            return;
          }
          
          auth_type.assign(header_val, matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
          auth_val.assign(header_val, matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);
          transform(auth_type.begin(), auth_type.end(), auth_type.begin(), ::toupper);
          
          scgi_env["AUTH_TYPE"] = auth_type;
          
          // TODO: implement digest auth if I have enough time!
          if (auth_type == "BASIC")
          {
            istringstream b64_val(auth_val);
            ostringstream user_and_pass;
            b64dec.decode(b64_val, user_and_pass);
            retval = regexec(&basic_val_pat, user_and_pass.str().c_str(), 3, matches, 0);
            if (0 != retval)
            {
              respond400BadReq(epollfd, state);
              return;
            }
            
            state->username.assign(user_and_pass.str().substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so));
            string password(user_and_pass.str().substr(matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so));
            DbTxn *txn(NULL);
            auth_db_env.txn_begin(NULL, &txn, 0);
            // Dbt key(static_cast<void *>(state->username.c_str()), state->username.length());
            // Dbt value;
            // NOTE: Maybe we could use const_cast here to avoid the memcpy.
            username_len = minimum<u_int32_t>(u_int32_t(minimum<size_t>(size_t(numeric_limits<u_int32_t>::max()), state->username.size())), BUF_SIZE);
            memcpy(temp_buf, state->username.data(), username_len);
            key.set_data(static_cast<void *>(temp_buf));
            key.set_flags(DB_DBT_USERMEM);
            key.set_size(username_len);
            key.set_ulen(username_len);
            value.set_data(static_cast<void *>(temp_buf_2));
            value.set_flags(DB_DBT_USERMEM);
            value.set_ulen(0);
            bool found_username(false);
            try
            {
              auth_db->get(txn, &key, &value, 0);
            }
            catch (DbMemoryException &e)
            {
              password_len = value.get_size();
              value.set_ulen(password_len);
              if (DB_NOTFOUND != auth_db->get(txn, &key, &value, 0))
                found_username = true;
            }
            if (found_username)
            {
              // Check that user's password matches given password.
              if (password == string(static_cast<char *>(value.get_data()), password_len))
              {
                pw_entry = getpwnam(state->username.c_str());
                // Check that user exists in system password database and has the correct GID.
                if (NULL == pw_entry)
                  fprintf(stderr, "Error: user \"%s\" does not exist in system password database\n", state->username.c_str());
                else if (projusers_gid != pw_entry->pw_gid)
                  fprintf(stderr, "Error: user \"%s\" has primary GID %d, but needs to have %d instead\n", state->username.c_str(), pw_entry->pw_gid, projusers_gid);
                else
                  valid_auth = true;
              }
              else
                fprintf(stderr, "Error: incorrect password \"%s\" given for user \"%s\"\n", password.c_str(), state->username.c_str());
            }
            else
              fprintf(stderr, "Error: user \"%s\" does not exist in server's authentication database\n", state->username.c_str());
            
            txn->commit(0);
          }
          else
          {
            respond501AuthNotImpl(epollfd, state);
            return;
          }
        }
      }
      else if ("CONTENT_TYPE" == header_name)
      {
        scgi_env[header_name] = header_val;
      }
      else
      {
        header_name.insert(0, "HTTP_");
        scgi_env[header_name] = header_val;
      }
    }
    
    if (require_content_length && !got_content_length)
    {
      respond400BadReq(epollfd, state);
      return;
      // goto PostWithoutContentLengthError; // issue "400 POST Requires Content-Length"?
    }
    
    if (!valid_auth)
    {
      respond401AuthReq(epollfd, state);
      return;
    }
    
    static const char SR_PIECE1[] = ":CONTENT_LENGTH" "\0";
    scgi_vars_tempbuf.assign(SR_PIECE1, sizeof(SR_PIECE1) - 1);
    if (require_content_length)
      scgi_vars_tempbuf.append(content_length_str);
    else
      scgi_vars_tempbuf.append("0");
    static const char SR_PIECE2[] = "\0" "SCGI" "\0" "1" "\0" "REQUEST_METHOD" "\0";
    scgi_vars_tempbuf.append(SR_PIECE2, sizeof(SR_PIECE2) - 1);
    scgi_vars_tempbuf.append(method);
    static const char SR_PIECE3[] = "\0" "REQUEST_URI" "\0";
    scgi_vars_tempbuf.append(SR_PIECE3, sizeof(SR_PIECE3) - 1);
    scgi_vars_tempbuf.append(uri);
    // Some important CGI env vars / SCGI headers are: CONTENT_TYPE (needed for POST form submission), REMOTE_USER, REMOTE_ADDR, AUTH_TYPE, SERVER_NAME, SERVER_PORT, SERVER_PROTOCOL.
    // But, some can be set by scgi_gateway.py (REMOTE_USER could be).
    static const char SR_PIECE4[] = "\0" "REMOTE_ADDR" "\0";
    char addr_str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(state->client_addr), addr_str, sizeof(addr_str));
    scgi_vars_tempbuf.append(SR_PIECE4, sizeof(SR_PIECE4) - 1);
    scgi_vars_tempbuf.append(addr_str);
    static const char SR_PIECE5[] = "\0" "SERVER_NAME" "\0";
    scgi_vars_tempbuf.append(SR_PIECE5, sizeof(SR_PIECE5) - 1);
    scgi_vars_tempbuf.append(listen_host);
    static const char SR_PIECE6[] = "\0" "SERVER_PORT" "\0";
    scgi_vars_tempbuf.append(SR_PIECE6, sizeof(SR_PIECE6) - 1);
    scgi_vars_tempbuf.append(listen_port);
    static const char SR_PIECE_SSL[] = "\0" "HTTPS" "\0";
    scgi_vars_tempbuf.append(SR_PIECE_SSL, sizeof(SR_PIECE_SSL) - 1);
    scgi_vars_tempbuf.append(https_indicator);
    // NOTE: SERVER_PROTOCOL and AUTH_TYPE come from scgi_env.
    map<string, string>::iterator iter(scgi_env.begin()), end(scgi_env.end());
    // Serialize the rest of the SCGI headers here.
    for (; end != iter; ++iter)
    {
      scgi_vars_tempbuf.append(1, '\0');
      scgi_vars_tempbuf.append(iter->first);
      scgi_vars_tempbuf.append(1, '\0');
      scgi_vars_tempbuf.append(iter->second);
    }
    static const char SR_PIECE7[] = "\0" ",";
    scgi_vars_tempbuf.append(SR_PIECE7, sizeof(SR_PIECE7) - 1);
    // Now we must calculate the total size of SCGI headers (NOT including the request body), and prepend it to the request.
    ostringstream from_number;
    from_number << (scgi_vars_tempbuf.size() - 2);
    state->scgi_vars_to_send.assign(from_number.str());
    state->scgi_vars_to_send.append(scgi_vars_tempbuf);
    // Initialize the count of vars bytes-written to 0.
    state->scgi_vars_bytes_written = 0;
    
    // Ask the process manager for a connection to our SCGI server.
    state->associated_fd = proc_mgr.getFdForUser(found_fd, pw_entry->pw_uid, state->username);
    if (-1 == state->associated_fd)
    {
      // We couldn't connect to SCGI server, so we must wait until the "alarm handler" is able to do that for us.
      state->phase = WAIT_FOR_SCGI;
      // We don't watch the client conn fd at all while in WAIT_FOR_SCGI phase.
      epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
    }
    else
    {
      // We are now connected to SCGI server.
      size_t length_to_move;
      
      if (require_content_length)
      {
        // This is a POST request. We will proceed to either FWD_REQUEST phase or FWD_RESPONSE phase, depending on whether the entirety of the request body (the POST data) has already been read from found_fd into incoming_data or not.
        
        /*
        // This is a POST request, so proceed to FWD_REQUEST phase.
        state->phase = FWD_REQUEST;
        */
        state->post_queue.clear(); // Initialize the CircBuf we will be writing into.
        // TODO: There could be a problem here. We need to ensure that *ALL* of the "body" (the POST data) gets to where the associated conn in SCGI_REQUEST_BODY will see it and send it (e.g. the post_queue circularbytebuffer). However, we likely have already read some of the beginning of the body from found_fd into state->incoming_data. What's more, it's possible that this read portion of the body is *all* of the body (and maybe even *past the end* of the body and into subsequent HTTP request(s)). And even more, it's possible that this data that we have read from found_fd is *all of the data* that the client is going to give us *until* it has received a response; this means that we can't even rely on receiving notification from epoll about this found_fd while it's in FWD_REQUEST phase!
        // Proposed solution:
        // 1) We calculate bytes_to_move = minimum((amount of bytes in "incoming_data"), (the Content-Length value)).
        // 2) We dequeue the first bytes_to_move unread bytes from incoming_data and enqueue them into post_queue. (This will be always succeed as long as the capacity of post_queue is at least ((the max size we use in attemptRecv) - 1 byte). That's because only data from the last recv remains in incoming_data at this point, and the "- 1 byte" because the last recv must have read at least the final byte of the \r\n\r\n separator.
        // 3) WARNING! At any time that a conn enters the SCGI_REQUEST_BODY phase, it *MUST* have its fd monitored for able-to-write *UNLESS* post_queue is empty. (Note that, because of the above rules, post_queue is likely to be non-empty at the time conn enters SCGI_REQUEST_BODY.)
        // Another WARNING: the above "Proposed solution" must be done *WHENEVER* a conn enters FWD_REQUEST phase!
        length_to_move = minimum<size_t>(state->incoming_data.size() - state->next_chr_to_read, state->post_content_length);
        // NOTE: This write to post_queue will ALWAYS write the entire "length_to_move" bytes; see above comment for the reason why.
        state->post_queue.write(state->incoming_data.substr(state->next_chr_to_read, length_to_move).data(), length_to_move);
        // NOTE: We must initialize post_bytes_read with the amount that we wrote to post_queue just now.
        state->post_bytes_read = length_to_move;
        // Now, we will cut off the initial portion of incoming_data which has alredy been read.
        state->incoming_data.erase(0, state->next_chr_to_read + length_to_move);
        state->next_chr_to_read = 0;
        if (length_to_move != state->post_content_length)
        {
          // There is more of the request body that needs to be read from found_fd. So, we must transition to FWD_REQUEST phase.
          state->phase = FWD_REQUEST;
          
          // Watch this client conn fd *only* when post_queue is *not* full (need to check now if it's full).
          if (state->post_queue.maxAmount() == length_to_move)
            // It's full, so temporarily stop watching.
            epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
        }
      }
      // We will transition to FWD_RESPONSE_HDR phase under either of two conditions:
      // 1) This is a GET request; or
      // 2) This is a POST request, and the entirety of the request body (the POST data) has already been read from found_fd and placed into post_queue.
      if ((!require_content_length) || (length_to_move == state->post_content_length))
      {
        state->phase = FWD_RESPONSE_HDR;
        state->response_queue.clear(); // Initialize the CircBuf we will be writing into.
        // While in FWD_RESPONSE phase, we will only be writing to the client conn fd.
        state->event.events = EPOLLOUT;
        // // However, watch the fd *only* when response_queue is *not* empty (and it's empty at first).
        // However, begin watching the fd *only* when our associated SCGI conn has prepared the response_hdr_to_send "linear queue".
        epoll_ctl(epollfd, EPOLL_CTL_DEL, found_fd, &(state->event));
      }
      // Because we are now connected to SCGI server, we must create an entry for the new SCGI conn in the fd_map.
      createSCGIConnState(state->associated_fd, found_fd, fd_map, epollfd);
    }

    // Record whether this client conn is doing a POST request or not.
    state->doing_post = require_content_length;
  }
}

// NOTE: for both parse error (400 Bad Request) and unsup. method (501 Method Not Implemented), should close the connection.

void respond401AuthReq(int epollfd, FdState *state)
{
  // Send "401 Authorization Required"
  state->phase = ERROR_NOCLOSE;
  state->response_text.assign("HTTP/1.1 401 Authorization Required\r\n"
                              "WWW-Authenticate: Basic realm=\"we only have one\"\r\n"
                              "Content-Type: text/html\r\n"
                              "Content-Length: 269\r\n"
                              "\r\n"
                              "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                              "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                              "<html>\n"
                              "<head>\n"
                              "<title>401 Authorization Required</title>\n"
                              "</head>\n"
                              "<body><h1>401 Authorization Required</h1></body>\n"
                              "</html>");
  state->error_response_bytes_written = 0;
  state->event.events = EPOLLOUT;
  epoll_ctl(epollfd, EPOLL_CTL_MOD, state->event.data.fd, &(state->event));
}

void respond400BadReq(int epollfd, FdState *state)
{
  // Send "400 Bad Request"
  state->phase = ERROR_CLOSE;
  state->response_text.assign("HTTP/1.1 400 Bad Request\r\n"
                              "Content-Type: text/html\r\n"
                              "Content-Length: 247\r\n"
                              "\r\n"
                              "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                              "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                              "<html>\n"
                              "<head>\n"
                              "<title>401 Bad Request</title>\n"
                              "</head>\n"
                              "<body><h1>401 Bad Request</h1></body>\n"
                              "</html>");
  state->error_response_bytes_written = 0;
  state->event.events = EPOLLOUT;
  epoll_ctl(epollfd, EPOLL_CTL_MOD, state->event.data.fd, &(state->event));
}

void respond501MethodNotImpl(int epollfd, FdState *state)
{
  // Send "501 Method Not Implemented"
  state->phase = ERROR_CLOSE;
  state->response_text.assign("HTTP/1.1 501 Method Not Implemented\r\n"
                              "Content-Type: text/html\r\n"
                              "Content-Length: 269\r\n"
                              "\r\n"
                              "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                              "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                              "<html>\n"
                              "<head>\n"
                              "<title>501 Method Not Implemented</title>\n"
                              "</head>\n"
                              "<body><h1>501 Method Not Implemented</h1></body>\n"
                              "</html>");
  state->error_response_bytes_written = 0;
  state->event.events = EPOLLOUT;
  epoll_ctl(epollfd, EPOLL_CTL_MOD, state->event.data.fd, &(state->event));
}

void respond501AuthNotImpl(int epollfd, FdState *state)
{
  // Send "501 Not Implemented"
  state->phase = ERROR_CLOSE;
  state->response_text.assign("HTTP/1.1 501 Not Implemented\r\n"
                              "Content-Type: text/html\r\n"
                              "Content-Length: 247\r\n"
                              "\r\n"
                              "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                              "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                              "<html>\n"
                              "<head>\n"
                              "<title>501 Not Implemented</title>\n"
                              "</head>\n"
                              "<body><h1>501 Not Implemented</h1></body>\n"
                              "</html>");
  state->error_response_bytes_written = 0;
  state->event.events = EPOLLOUT;
  epoll_ctl(epollfd, EPOLL_CTL_MOD, state->event.data.fd, &(state->event));
}
